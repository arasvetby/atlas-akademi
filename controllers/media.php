<?php

class media extends controller{
      public function __construct($method="dashboard") {
        parent::__construct();
        
        $this->model = $this->load->model("panel_m");
        
        //Get, Post ve Files verilerini topluyoruz
        $this->data = array_merge($this->data, $_POST);
        $this->data = array_merge($this->data, $_GET);
        $this->data = array_merge($this->data, $_FILES);
        
        if (!isset($this->data["select"]))
              $this->data["select"]="";
        
        if(method_exists($this, $method))
            $this->{$method}();
        else     
            echo "404";
        
      }
      
      private $data = array();
      private $model = null;


      public function dashboard() {
          
          
          $this->data["veri"]= $this->model->galery();
          echo $this->load->view("media", $this->data);
          
      }
       public function ckmedia() {
          
          
          $this->data["veri"]= $this->model->galery();
          echo $this->load->view("ckmedia", $this->data);
          
      }
      public function source() {
          
          
        if(isset($this->data["media-name"]))
            $this->data["veri"]= $this->model->galery(array("name"=> $this->data["media-name"]));
        else 
            $this->data["veri"]= $this->model->galery();
        
       
        echo $this->load->view("html/media", $this->data);
          
      }
      
      public function load() {
         
          
          $files= $this->data["files"];
          
          $image = new image();
          
          for ($index = 0; $index < count($files["name"]); $index++) {
              if ($files["error"][$index]==0) {
                  
                    $data=array(
                        
                        "name"=>$files["name"][$index],
                        "title"=>$files["name"][$index],
                        "alt"=>$files["name"][$index]
                    );
                  
                    switch ($files["type"][$index]) { 
                        case "image/gif": 
                            $image->load($files["tmp_name"][$index]);
                           
                            
                            $data = array_merge($data,
                                    array(
                                        "url"=>"media/".md5(uniqid()).".gif",
                                        "width"=>$image->getWidth(),
                                        "height"=>$image->getHeight(),
                                        "is_image"=>1,
                                        "size"=>$files["size"][$index]
                                        ));
                            $this->model->media($data);
                            $image->save($data["url"],IMAGETYPE_GIF);
                            break; 
                        case "image/jpeg": 
                            $image->load($files["tmp_name"][$index]);
                           
                            
                            $data = array_merge($data,
                                    array(
                                        "url"=>"media/".md5(uniqid()).".jpg",
                                        "width"=>$image->getWidth(),
                                        "height"=>$image->getHeight(),
                                        "is_image"=>1,
                                        "size"=>$files["size"][$index]
                                        ));
                            $this->model->media($data);
                            $image->save($data["url"]);
                            break; 
                        case "image/png": 
                            $image->load($files["tmp_name"][$index]);
                           
                            
                            $data = array_merge($data,
                                    array(
                                        "url"=>"media/".md5(uniqid()).".png",
                                        "width"=>$image->getWidth(),
                                        "height"=>$image->getHeight(),
                                        "is_image"=>1,
                                        "size"=>$files["size"][$index]
                                        ));
                            $this->model->media($data);
                            $image->save($data["url"],IMAGETYPE_PNG);
                            break; 
                        
                        default :
                            $data = array_merge($data,
                                    array(
                                        "url"=>"media/".md5(uniqid())."_".$files["name"][$index],
                                        "is_image"=>0,
                                        "size"=>$files["size"][$index]
                                        ));
                            $this->model->media($data);
                            
                            move_uploaded_file($files["tmp_name"][$index], $data["url"]);
                            break;
                    } 
                 
              }
          }
          
          $this->data["veri"]= $this->model->galery();
          echo $this->load->view("html/media", $this->data);
          
      }
    
      public function edit() {
           $temp=array(
               "no"=> $this->data["no"],
               "detail"=> $this->data["detail"],
              
               "name"=> $this->data["name"],
               "title"=> $this->data["title"],
               "alt"=> $this->data["alt"],
               "width"=> $this->data["width"],
               "height"=> $this->data["height"]
           );
                  
          
           $return = $this->model->media($temp,"edit");
           
           if ($return==1) {
               $this->data["veri"]= $this->model->galery();
               echo $this->load->view("html/media", $this->data);
           }else{
               echo 'Kayıt işlemi başarısız!';
               return http_response_code(404);
           }
      }
      
    private function delete() {
       
        $return = $this->model->delete("media", $this->data["detail"]);
        
         if ($return==1) {
           
            $this->data["veri"]= $this->model->galery();
            echo $this->load->view("html/media", $this->data);

        }else{
            echo 'Kayıt işlemi başarısız!';
            return http_response_code(404);
        }
        
    }
}