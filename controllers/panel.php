<?php

class panel extends controller{
    public function __construct($method="dashboard") {
        parent::__construct();
        
        $this->model = $this->load->model("panel_m");
        
        //Get, Post ve Files verilerini topluyoruz
        $this->data = array_merge($this->data, $_POST);
        $this->data = array_merge($this->data, $_GET);
        $this->data = array_merge($this->data, $_FILES);
        
        
        //Kullanıcı girişi kontrolu
        if (!session::get("login") && (!isset($_COOKIE["login"]) || !$_COOKIE["login"])) {
            $this->login();
        } else {
            if(method_exists($this, $method))
                $this->{$method}();
            else     
                echo "404";
        }
       
        
        
                  
     
        
    }
    
    private $data = array();
    private $model = null;

    private function login() {
        
        if (isset($this->data["status"]) && $this->data["status"]==1) {
            
            if (!isset($this->data["username"]) || empty($this->data["username"]) || !isset($this->data["password"]) || empty($this->data["password"]) ) {
                echo $this->load->view("login",array("message"=>2));
            } else {
                $temp = $this->model->login($this->data["username"], $this->data["password"]);
                if ($temp!=false) {
                    if (isset($this->data["remember"]) && $this->data["remember"]==1) {
                        
                    }else{
                        
                                               
                        session::set("login", true);
                        session::set("username", $temp["username"]);
                        session::set("no", $temp["no"]);
                        session::set("image", $temp["image_file"]);
                        session::set("date", $temp["update_date"]);
                        
                    }
                    
                    echo $this->load->view("login",array("message"=>3));
                    
                }else{
                    echo $this->load->view("login",array("message"=>1));
                }
            }
            
        }else{
            echo $this->load->view("login",array("message"=>0));
        }
       
    }

    private function logout()
    {
        session::destroy();
        echo $this->load->view("login",array("message"=>0));
    }

    private function dashboard() {
        echo $this->load->view("header", $this->data);
        echo $this->load->view("dashboard");
        echo $this->load->view("footer", $this->data);
    }
    
    private function back() {
        echo $this->load->view("dashboard");
    }
    /*Sayfa oluşturma işlemleri başlangıç*/
    
    private function table() {
        $module= $this->load->data();
        if($module[$this->data["module"]]["table"]=="content")
            $this->data["veri"] = $this->model->table($module[$this->data["module"]],array("module"=> $this->data["module"])); 
        else
            $this->data["veri"] = $this->model->table($module[$this->data["module"]]); 
        
        echo $this->load->view("table", $this->data);
    }
    
    private function page() {
        $module= $this->load->data();
        if (isset($this->data["no"])) {
            $this->data["veri"] = $this->model->table($module[$this->data["module"]],array("no"=> $this->data["no"]))[0]; 
        }
        else if ($module[$this->data["module"]]["type"]=="page"){
            
            $temp = $this->model->table($module[$this->data["module"]],array("module"=> $this->data["module"]));
            if (count($temp)>0) 
                $this->data["veri"] = $temp[0];
            else
                $this->data["veri"] = $this->model->table($module[$this->data["module"]],array("no"=> ""))[0]; 
            
        }
        else{
            $this->data["veri"] = $this->model->table($module[$this->data["module"]],array("no"=> ""))[0]; 
        }
        echo $this->load->view("page", $this->data);
    }
    private function modal() {
        $module= $this->load->data();
        if (isset($this->data["no"])) {
            $this->data["veri"] = $this->model->table($module[$this->data["module"]],array("no"=> $this->data["no"]))[0]; 
        }else{
            $this->data["veri"] = $this->model->table($module[$this->data["module"]],array("no"=> ""))[0]; 
        }
       
        echo $this->load->view("modal", $this->data);
    }
    private function detail() {
        if (isset($this->data["no"])) {
            $this->data["veri"] = $this->model->description($this->data["no"])[0]; 
        }
        echo $this->load->view("detail", $this->data);
    }
    
    private function add(){
        
        $module= $this->load->data();
        
        foreach ($module[$this->data["module"]]["elements"] as $value) {
            if ($value["null"]==false && empty($this->data[$value["name"]])) {
                
                echo $value["label"]. " alanı boş bırakılamaz!";
                return http_response_code(404);
            }
        }
        
       
        
        $return = 0;
        if (empty($this->data["no"])) {
            $return = $this->model->add($module[$this->data["module"]], $this->data);
        }else{
            $return = $this->model->edit($module[$this->data["module"]], $this->data);
        }
        
        if ($return==1) {
            if ($module[$this->data["module"]]["type"]=="table") {
                if($module[$this->data["module"]]["table"]=="content")
                    $this->data["veri"] = $this->model->table($module[$this->data["module"]],array("module"=> $this->data["module"])); 
                else
                    $this->data["veri"] = $this->model->table($module[$this->data["module"]]); 

                
                if ($module[$this->data["module"]]["add"]=="modal")
                    echo $this->load->view("html/table", $this->data);
                else
                    echo $this->load->view("table", $this->data);
                
            }else{
                $temp = $this->model->table($module[$this->data["module"]],array("module"=> $this->data["module"]));
                if (count($temp)>0) 
                    $this->data["veri"] = $temp[0];
                else
                    $this->data["veri"] = $this->model->table($module[$this->data["module"]],array("no"=> ""))[0]; 
            
       
                echo $this->load->view("page", $this->data);
            }
            
        }else{
            echo 'Kayıt işlemi başarısız!';
            return http_response_code(404);
        }
        
        
    }
    
    private function source() {
        $module= $this->load->data();
        
        $temp =array();
        
        foreach ($module[$this->data["module"]]["elements"] as $value) {
            if ($value["filter"]==true && !empty($this->data[$value["name"]])) {
                $temp=array_merge($temp,array($value["name"]=>$this->data[$value["name"]]));
            }
        }
        if($module[$this->data["module"]]["table"]=="content")
            $temp = array_merge ($temp,array("module"=> $this->data["module"])); 
        
        
        $this->data["veri"] = $this->model->table($module[$this->data["module"]],$temp); 
        echo $this->load->view("html/table", $this->data);
    }

    private function delete() {
        $module= $this->load->data();
        $return = $this->model->delete($module[$this->data["module"]]["table"], $this->data["detail"]);
        
         if ($return==1) {
           
            if($module[$this->data["module"]]["table"]=="content")
                $this->data["veri"] = $this->model->table($module[$this->data["module"]],array("module"=> $this->data["module"])); 
            else
                $this->data["veri"] = $this->model->table($module[$this->data["module"]]); 
           
            echo $this->load->view("html/table", $this->data);
           
            
        }else{
            echo 'Kayıt işlemi başarısız!';
            return http_response_code(404);
        }
        
    }
     private function status() {
        $module= $this->load->data();
        $return = $this->model->status($module[$this->data["module"]]["table"], $this->data["detail"], $this->data["status"]);
        
         if ($return==1) {
           
            if($module[$this->data["module"]]["table"]=="content")
                $this->data["veri"] = $this->model->table($module[$this->data["module"]],array("module"=> $this->data["module"])); 
            else
                $this->data["veri"] = $this->model->table($module[$this->data["module"]]); 
            echo $this->load->view("html/table", $this->data);
           
            
        }else{
            echo 'Durum güncelleme başarısız!';
            return http_response_code(404);
        }
        
    }
    
     private function order() {
        $module= $this->load->data();
        $return = $this->model->order($module[$this->data["module"]]["table"], $this->data["detail"], $this->data["order"]);
        
         if ($return==1) {
           
            if($module[$this->data["module"]]["table"]=="content")
                $this->data["veri"] = $this->model->table($module[$this->data["module"]],array("module"=> $this->data["module"])); 
            else
                $this->data["veri"] = $this->model->table($module[$this->data["module"]]); 
            
            echo $this->load->view("html/table", $this->data);
           
            
        }else{
            echo 'Sıralama güncelleme başarısız!';
            return http_response_code(404);
        }
        
    }

    /*Sayfa oluşturma işlemleri bitiş*/
    
    
   
}

?>