<?php

class main extends controller{
    public function __construct($method="index") {
        parent::__construct();
        
        $this->model = $this->load->model("main_m");
        
        //Get, Post ve Files verilerini topluyoruz
        $this->data = array_merge($this->data, $_POST);

        $this->data = array_merge($this->data, $_GET);
        $this->data = array_merge($this->data, $_FILES);
        
        
        $this->module = $this->load->data();
        
        $this->data["active_link"] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $this->data["menu"]=$this->model->select($this->module["menu"],array("module"=>"menu"));
        $this->data["sub_link"]=array();



        foreach ($this->data["menu"] as $value) {
            if ($value["type-select"]==3) {
                $this->data["type_".$value["category-select"]]=$this->model->select($this->module["category"],array("no"=>$value["category-select"]),"single",1);
                $this->data["menu_".$value["category-select"]]=$this->model->select($this->module["page"],array("category"=>$value["category-select"]));
                
            }
            if ($value["level-select"]==2) {

               if($value["type-select"]==1)
                    array_push( $this->data["sub_link"],array("no"=>$value["sub-select"],"link"=>$value["link"],"title"=>$value["title"]));
               else if($value["type-select"]==2)
                    array_push( $this->data["sub_link"],array("no"=>$value["sub-select"],"link"=>url.$value["page-sluq"],"title"=>$value["page"]));
               else if($value["type-select"]==3)
                    array_push( $this->data["sub_link"],array("no"=>$value["sub-select"],"link"=>url.$value["category-sluq"],"title"=>$value["title"]));
              

               
            }
        }

       
       
       
        $this->data["social"]= $this->model->select($this->module["social"],array("module"=>"social"),"single",1);
        $this->data["setting"]= $this->model->select($this->module["setting"],array("module"=>"setting"),"single",1);
        
        //Meta etiketleri
        $this->data["meta_author"]="";
        $this->data["meta_type"]="";
        $this->data["meta_image"]="";
        $this->data["meta_url"]=$this->data["active_link"];
        $this->data["meta_keywords"]="";
        $this->data["meta_description"]="";
     
       
        
        
        if(method_exists($this, $method))
            $this->{$method}();
        else     
            echo "404";
        
       
        
        
                  
     
        
    }
    private $module= array();
    private $data = array();
    private $model = null;

    
   
    
    private function index(){
    
       $this->data["static"]=$this->model->select($this->module["static"],array("module"=>"static"),"single",1);

        
       $this->data["slider"]=$this->model->select($this->module["slider"],array("module"=>"slider"),"list",6); 

       
      
       $this->data["about1"]=$this->model->select($this->module["page"],array("no"=>$this->data["static"]["about1-select"]),"single",1);
       $this->data["about2"]=$this->model->select($this->module["page"],array("no"=>$this->data["static"]["about2-select"]),"single",1); 
       //$this->data["hakkimizda"]=$this->model->select($this->module["page"],array("no"=>$this->data["static"]["about-select"]),"single",1);
       $this->data["hizmetlerimiz"]=$this->model->select($this->module["page"],array("category"=>$this->data["static"]["bottom1-select"]),"list",6);
       $this->data["blog"]=$this->model->select($this->module["page"],array("category"=>$this->data["static"]["bottom2-select"]),"list",3);

  
      


       echo $this->load->html("header", $this->data);
       echo $this->load->html("main", $this->data);
       echo $this->load->html("footer", $this->data);          
    }
    
    
    public function page()
    {

       


        if($this->data["controller"]!="contact" )
            $temp=$this->model->select("content",array("sluq"=>$this->data["controller"]),"single",1,$this->module); 


        
        if($this->data["controller"]=="contact"){
            $this->data["hTitle"]="İletişim";

            

            echo $this->load->html("header", $this->data);
            echo $this->load->html("contact", $this->data);
        }else if($temp["module"]=="page" || $temp["module"]=="galeri"){
            $this->data["page"]=$temp;          
            $this->data["hTitle"]=$temp["title"];

            $this->data["meta_author"]=$temp["author"];
            $this->data["meta_type"]=$temp["og-type"];
            
            $this->data["meta_image"]=(isset($temp["image_file"]))?$temp["image_file"]:"";
            $this->data["meta_keywords"]=$temp["keywords"];
            $this->data["meta_description"]=$temp["description"];

            $this->data["category"] = $this->model->select($this->module["category"],array("no"=>$temp["category"]),"single",1); 

            $this->data["contents_list"]=$this->model->select($this->module[$temp["module"]],array("category"=>$temp["category"]),"list",12); 
            $this->data["contents"]=$this->model->select($this->module[$temp["module"]],array("category"=>$temp["category"]),"list",6); 

           
            echo $this->load->html("header", $this->data);
           
            echo $this->load->html($temp["module"], $this->data);
        
        }else{
            if(!isset($this->data["path"]))
                $this->data["path"]=1;

            $this->data["category"] = $temp;
            if($this->data["category"]["type-select"]==1)
                $this->data["page"]=$this->model->select($this->module["page"],array("category"=>$temp["no"]),"list");
            else if($this->data["category"]["type-select"]==3)    
                $this->data["page"]=$this->model->select($this->module["galeri"],array("category"=>$temp["no"]),"list");
            else
                $this->data["page"]=$this->model->select($this->module["page"],array("category"=>$temp["no"]),"list",(($this->data["path"]-1)*6).",6");
            

            
            $count=ceil(count($this->model->select($this->module["page"],array("category"=>$temp["no"])))/6);
            $this->data["hTitle"]=$temp["title"];

            $this->data["pages"]=array("link"=>url.$this->data["controller"],"count"=>$count,"active"=>$this->data["path"]);

           
            echo $this->load->html("header", $this->data);

           
            if($this->data["category"]["type-select"]==1)
                echo $this->load->html("category", $this->data);
            else
                echo $this->load->html("list", $this->data);
        }
        
        echo $this->load->html("footer", $this->data);
    }

    public function news(){
        $this->data["hTitle"]="Arama Sonuçları";
        if(!isset($this->data["p"]))
                $this->data["p"]=1;
            
        $this->data["category"]=array("title"=>"Arama Sonuçları");        
        $count=0;
        $this->data["page"]=array();
      

        $temp = $this->model->select($this->module["page"],array());

       
        foreach ($temp as $value) {
            

            if (isset($value["labels"] ) && (strpos($value["labels"],$this->data["path"]) !==false || strpos($this->slugify($value["labels"]),$this->data["path"])!==false)){
                if($count<6)
                    array_push($this->data["page"],$value);
                $count++;
            }
        }

        $count=ceil($count/6);

     
        $this->data["pages"]=array("link"=>url."news/".$this->data["path"],"count"=>$count,"active"=>$this->data["p"]);


        echo $this->load->html("header", $this->data);
        echo $this->load->html("list", $this->data);
        echo $this->load->html("footer", $this->data);
    }
    public function send(){
        
        $temp = $this->model->select($this->module["mail"],array("module"=>"mail"),"single",1);

       

        require("./libs/smtp/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 1; 
        $mail->SMTPAuth = true; 
        $mail->SMTPSecure = $temp["secure"];
        $mail->Host = $temp["host"]; 
        $mail->Port = $temp["port"]; 
        $mail->IsHTML(true);
        //$mail->SetLanguage("tr", "phpmailer/language");
        $mail->CharSet  ="utf-8";
        $mail->Username = $temp["mail"]; 
        $mail->Password = $temp["password"]; 
        $mail->SetFrom($this->data["email"], $this->data["name"]); 

        foreach (explode(',',$temp["send"]) as $value)
            $mail->AddAddress($value,$value); 

        if (!empty($temm["cc"])){
            foreach (explode(',',$temp["cc"]) as $value)
                $mail->AddCC($value,$value);
        }
        if (!empty($temm["bcc"])){
            foreach (explode(',',$temp["bcc"]) as $value)
                $mail->AddBCC($value,$value);
        }

        $mail->Subject = $this->data["subject"]; // Email konu başlığı
        $mail->Body = $this->data["body"]; // Mailin içeriği
        if(!$mail->Send()){
            
            echo "Beklenmeyen Bir Hata Oluştu!";
         
        } else {
            echo "İleti Başarıyla Gönderildi...";
        }
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}