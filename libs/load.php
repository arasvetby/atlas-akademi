<?php

class load {

     public function html($fileName, $data = false){
        if($data == true){
            extract($data);
        }
        include "views/html/" . $fileName . ".php";
    }
    
    public function view($view,$data = array()){

        
        // config içerisindeki sabitleri değişkenlere atıyoruz
        $url = url;
        $style = style;
        $media = media;
        $username=session::get("username");
        $image=session::get("image");
        $update=session::get("date");
        
        $modules = $this->data();
        //module dosyalarını alıyoruz
        if (isset($data["module"])) {
            $data = array_merge($data, $modules[$data["module"]]);
        } else {
            $data = array_merge($data, $this->data("startup"));
        }
        
       
        extract($data);
        
        //detail için veriyi parçalıyoruz
        if ($view=="detail") {
            extract($veri);
        }
        
        ob_start();
        include "views/panel/".$view.".php";
        $output = ob_get_clean();

        //Değişkenleri ekliyoruz
        preg_match_all("/{{(.*?)}}/", $output,$subject);
        
       
        
        for ($index = 0; $index < count($subject[0]); $index++) {
            
            switch ($subject[1][$index]) {
                
                case "filter":
                    $output = str_replace($subject[0][$index], $this->view("html/filter",$data), $output);
                    break;
                case "filter-elements":
                    
                    $temp="";
                    foreach ($elements as $value) {
                        if ($value["filter"]==true) {
                            
                             if ($value["file"]=="select") {
                                if(isset($veri[$value["name"]."-option"]))
                                    $value["option"]=$veri[$value["name"]."-option"];
                                if(isset($veri[$value["name"]."-select"]))
                                    $value["select"]=$veri[$value["name"]."-select"];
                                $option="";
                                foreach ($value["option"] as $k => $v) {
                                    if ($k==$value["select"]) 
                                        $option.="<option value='$k' selected>$v</option>";
                                    else
                                        $option.="<option value='$k'>$v</option>";
                                }
                                $value["option"]=$option;
                            }
                            
                            $value["id"]="filter_".$value["id"];

                            $temp .="<td>". $this->view("html/".$value["file"],$value)."</td>";
                        }
                    }
                    $output = str_replace("{{filter-elements}}", $temp, $output);
                    break;
                case "modal-elements":
                    $temp="";
                    
                    foreach ($elements as $value) {
                        if(isset($veri[$value["name"]]))
                            $value["value"]=$veri[$value["name"]];
                        
                        if ($value["file"]=="title") {
                            if(isset($veri["sluq"]))
                                $value["sluq"]=$veri["sluq"];
                        }
                        
                        if ($value["file"]=="select" || $value["file"]=="type") {
                            
                                if(isset($veri[$value["name"]."-option"]))
                                    $value["option"]=$veri[$value["name"]."-option"];
                                if(isset($veri[$value["name"]."-select"]))
                                    $value["select"]=$veri[$value["name"]."-select"];
                                $option="";
                                foreach ($value["option"] as $k => $v) {
                                    if ($k==$value["select"]) 
                                        $option.="<option value='$k' selected>$v</option>";
                                    else
                                        $option.="<option value='$k'>$v</option>";
                                }
                                $value["option"]=$option;

                                if($value["file"]=="type"){
                                    $effect = "{";
                                    foreach ($value["effect"] as $k => $v) {
                                        $effect.="'".$k."':'".$v."',";
                                    }
                                    $effect=rtrim($effect,",");
                                    $effect.="}";
                                    $value["effect"]=$effect;
                                }
                                    
                            }
                        
                         

                        $temp .= $this->view("html/".$value["file"],$value);
                    }
                  
                    $output = str_replace("{{modal-elements}}", $temp, $output);
                    break;
                case "page-elements-left":
                    $temp="";
                    
                    foreach ($elements as $value) {
                        if ($value["col"]=="left") {
                            if(isset($veri[$value["name"]]))
                            $value["value"]=$veri[$value["name"]];
                        
                            if ($value["name"]=="title") {
                                if(isset($veri["sluq"]))
                                    $value["sluq"]=$veri["sluq"];
                            }
                            
                            if ($value["file"]=="select" || $value["file"]=="type") {
                                if(isset($veri[$value["name"]."-option"]))
                                    $value["option"]=$veri[$value["name"]."-option"];
                                if(isset($veri[$value["name"]."-select"]))
                                    $value["select"]=$veri[$value["name"]."-select"];
                                $option="";
                                foreach ($value["option"] as $k => $v) {
                                    if ($k==$value["select"]) 
                                        $option.="<option value='$k' selected>$v</option>";
                                    else
                                        $option.="<option value='$k'>$v</option>";
                                }
                                $value["option"]=$option;
                                if($value["file"]=="type"){
                                    $effect = "{";
                                    foreach ($value["effect"] as $k => $v) {
                                        $effect.="'".$k."':'".$v."',";
                                    }
                                    $effect=rtrim($effect,",");
                                    $effect.="}";
                                    $value["effect"]=$effect;
                                }
                            }

                            if($value["file"]=="full-image"){
                                if(isset($veri["text-".$value["name"]])){
                                    $value["text_value"]=$veri["text-".$value["name"]];
                                }
                                if(isset($veri["textarea-".$value["name"]])){
                                    $value["textarea_value"]=$veri["textarea-".$value["name"]];
                                }    
                            }
                            
                            $temp .= $this->view("html/".$value["file"],$value);
                        }
                    }
                  
                    $output = str_replace("{{page-elements-left}}", $temp, $output);
                    break;
                case "page-elements-right":
                    $temp="";
                    
                    foreach ($elements as $value) {
                        if ($value["col"]=="right") {
                            if(isset($veri[$value["name"]]))
                                $value["value"]=$veri[$value["name"]];
                        
                            if ($value["name"]=="title") {
                                if(isset($veri["sluq"]))
                                    $value["sluq"]=$veri["sluq"];
                            }
                            
                            if ($value["file"]=="select" || $value["file"]=="type") {
                                if(isset($veri[$value["name"]."-option"]))
                                    $value["option"]=$veri[$value["name"]."-option"];
                                if(isset($veri[$value["name"]."-select"]))
                                    $value["select"]=$veri[$value["name"]."-select"];
                                $option="";
                                foreach ($value["option"] as $k => $v) {
                                    if ($k==$value["select"]) 
                                        $option.="<option value='$k' selected>$v</option>";
                                    else
                                        $option.="<option value='$k'>$v</option>";
                                }
                                $value["option"]=$option;
                                if($value["file"]=="type"){
                                    $effect = "{";
                                    foreach ($value["effect"] as $k => $v) {
                                        $effect.="'".$k."':'".$v."',";
                                    }
                                    $effect=rtrim($effect,",");
                                    $effect.="}";
                                    $value["effect"]=$effect;
                                }
                            }
                            
                            
                            $temp .= $this->view("html/".$value["file"],$value);
                        }
                    }
                  
                    $output = str_replace("{{page-elements-right}}", $temp, $output);
                    break;   
                case "table":
                    $output = str_replace($subject[0][$index], $this->view("html/table",$data), $output);
                    break;
                case "table-title":
                    
                    $temp="";
                    if ($sort) {
                            $temp.="<th width='%10'>Gösterim Sırası</th>";
                        }
                    foreach ($elements as $value) {
                        
                        if ($value["table"]==true) {
                            
                            $temp .="<th>". $value["label"] ."</th>";
                        }
                    }
                    $temp.="<th width='5%'>Durum</th>";
                    $temp.="<th width='17%'>İşlemler</th>";
                    $output = str_replace("{{table-title}}", $temp, $output);
                    break;
                case "table-data":
                    
                    $temp="";
                    $order =0;
                    foreach ($veri as $row) {
                        
                            
                        $temp.="<tr>";

                        if ($sort) {
                            $order++;
                            $temp.="<td><input id='orderby_".$row['detail']."' type='number' class='form-control text-center onlyNumber sortInput' min='1' value='$order'><a onclick=\"SaveOrder('$url?controller=panel&path=order&module=$module', '".$row['detail']."')\" class='btn btn-primary btn-block sortButton'><i class='fas fa-save'></i></a></td>";
                        }

                        foreach ($elements as $value) {
                            if ($value["table"]==true) {

                                $temp .="<td>";
                                if (isset($row[$value["name"]])) {
                                    if ($value["file"]=="image") 
                                        $temp.="<a id='disabledButton' style='cursor:default;' href='".$row[$value["name"]."_file"]."' data-spzoom><img src='".$row[$value["name"]."_file"]."' style='margin:0 auto; display:block;' width='50' height='50' class='img-thumbnail' onerror='imageError(this)'></a>";
                                    else
                                        $temp.=$row[$value["name"]];
                                }
                                     
                                $temp .="</td>";
                            }
                        }



                        $temp.=($row["is_active"]==true)?"<td><label class='switch'><input onclick=\"StatusData('$url?controller=panel&path=status&module=$module&detail=".$row['detail']."&status=0')\"  type='checkbox' class='switch' id='switch-id' checked> <span class='slider'></span> </label></td>":"<td><label class='switch'><input onclick=\"StatusData('$url?controller=panel&path=status&module=$module&detail=".$row['detail']."&status=1')\"  type='checkbox' class='switch' id='switch-id' > <span class='slider'></span> </label></td>";
                        $temp.="<td class='text-center'><a onclick=\"GetPage('$url?controller=panel&path=detail&module=$module&no=".$row['detail']."','detail')\" class='btn btn-info' data-toggle='modal' data-target='#detail-modal' title='Detaylar'><i class='fas fa-info-circle'></i> <span class='dNone'>Detaylar</span></a> "
                                ."<a onclick=\"GetPage('$url?controller=panel&path=".$add."&module=$module&no=".$row['no']."','$id')\" class='btn btn-warning'  title='Düzenle'><i class='far fa-edit'></i><span class='dNone'>Düzenle</span></a> "
                                ."<a onclick=\"DeleteData('$url?controller=panel&path=delete&module=$module&detail=".$row['detail']."')\" class='btn btn-danger' title='Sil'><i class='fas fa-trash-alt'></i><span class='dNone'> Sil</span></a></td>";
                        $temp.="</tr>";
                        
                    }
                    
                    $output = str_replace("{{table-data}}", $temp, $output);
                    break;
                case "media-main":
                    $output = str_replace($subject[0][$index], $this->view("html/media",$data), $output);
                    break;
                case "list-media":
                    
                    $temp="";
                    
                    foreach ($veri as $value) {
                        if ($value["is_image"]) {
                            $temp.='<li class="searchMedia">
                                    <div class="col-md-2">
                                        <div class="mediaBorder">
                                            <img src="'.$value["url"].'" alt="'.$value["alt"].'" title="'.$value["title"].'" url="'.$value["url"].'" class="img-responsive" width="'.$value["width"].'" height="'.$value["height"].'" upload_date="'.$value["update_date"].'" detail="'.$value["detail"].'" imageno="'.$value["no"].'" size="'.$value["size"].'" onerror="imageError(this)">
                                            <span>'.$value["name"].'</span>
                                        </div>
                                    </div>
                                </li>';
                        } else {
                            $temp.='<li class="searchMedia">
                                    <div class="col-md-2">
                                        <div class="mediaBorder">
                                            <img src="media/folder.png" alt="'.$value["alt"].'" title="'.$value["title"].'" url="'.$value["url"].'" class="img-responsive" width="0" height="0" imageno="'.$value["no"].'" upload_date="'.$value["update_date"].'" detail="'.$value["detail"].'" size="'.$value["size"].'" onerror="imageError(this)">
                                            <span>'.$value["name"].'</span>
                                        </div>
                                    </div>
                                </li>';
                        }
                        
                           
                    }
                     $output = str_replace("{{list-media}}", $temp, $output);
                    break;
                case "menu-list":
                    $output = str_replace($subject[0][$index], $this->view("menu",$data), $output);
                    break;
                case "menu-content":
                    
                    $temp="";
                    foreach ($modules as $key => $value) {
                        if ($value["menu"]=="content") {
                            
                            $temp .="<li><a onclick=\"GetPage('".url."?controller=panel&path=".$value["type"]."&module=".$key."','main')\"><i class='fa fa-".$value["icon"]."'></i> <span>".$value["title"]."</span></a></li>";
                        }
                    }
                    $output = str_replace("{{menu-content}}", $temp, $output);
                    break;
                case "menu-settings":
                    
                    $temp="";
                    foreach ($modules as $key => $value) {
                        if ($value["menu"]=="settings") {
                            
                            $temp .="<li><a onclick=\"GetPage('".url."?controller=panel&path=".$value["type"]."&module=".$key."','main')\"><i class='fa fa-".$value["icon"]."'></i> <span>".$value["title"]."</span></a></li>";
                        }
                    }
                    $output = str_replace("{{menu-settings}}", $temp, $output);
                    break;
                default:
                    $output = str_replace($subject[0][$index],${$subject[1][$index]}, $output);
                    break;
            }
            
            
        }
        
       
       
        
        //Veriyi ekrana yazıyoruz
        return $output;

    }


    public function model($model){
            include "models/".$model.".php";
            return new $model();
    }
    
    public function data($name="module") {
        
        $data = file_get_contents(path.$name.".json"); 
        $wizards = json_decode($data, true);
        return $wizards;
        
    }
    
}
?>