<?php

class Database extends PDO{
    
    public function __construct($dsn, $user, $password) {
        parent::__construct($dsn, $user, $password);
        $this->query("SET NAMES ´utf8´");
        $this->query("SET CHARACTER SET utf8");
    }
    
    public function table($name, $no = false, $fetchMode = PDO::FETCH_ASSOC){
        
        $sql = "SELECT t.no, t.text, t.detail, d.is_active, d.is_delete, d.order_by FROM ". $name ." AS t LEFT JOIN detail AS d ON t.detail = d.no where d.is_delete=0";
        
        if ($no == true) {
            $sql.=" and t.no = '$no'";
        }
        
        $sql.=" order by d.order_by, d.update_date desc";
        
        $sth = $this->prepare($sql);
        $sth->execute();
//        echo $sth->errorCode();
//        echo '<br>';
//        print_r($sth->errorInfo());
        
       
        
        return $sth->fetchAll($fetchMode);
    }
     public function insert($table, $data){
        $fieldKeys = implode(",", array_keys($data));
        $fieldValues = ":" . implode(", :", array_keys($data));

        $sql = "INSERT INTO $table ($fieldKeys) VALUES ($fieldValues)";
        $sth = $this->prepare($sql);
        foreach ($data as $key => $value) {
            
            $sth->bindValue(":$key", str_replace("'","\'",$value));
        }
        $return=$sth->execute();
//        echo $sth->errorCode();
//        echo '<br>';
//       print_r($sth->errorInfo());
        return $return;
    }
    
    public function update($table, $data, $no){
        $updateKeys = null;
        foreach ($data as $key => $value) {
            $updateKeys .= "$key=:$key,";
        }
        $updateKeys = rtrim($updateKeys, ",");
        $sql = "UPDATE $table SET $updateKeys WHERE no='$no'";
        
        $sth = $this->prepare($sql);
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", str_replace("'","\'",$value));
        }
        $return=$sth->execute();
//        echo $sth->errorCode();
//        echo '<br>';
//        print_r($sth->errorInfo());
        return $return;
    }
    
    public function identy($table , $fetchMode = PDO::FETCH_ASSOC){
        $sql="SELECT id FROM $table ORDER BY id DESC LIMIT 1";
       
        $sth = $this->prepare($sql);
        $sth->execute();
//        echo $sth->errorCode();
//        echo '<br>';
//        print_r($sth->errorInfo());
        return $sth->fetch($fetchMode);
    }

    public function image($no , $fetchMode = PDO::FETCH_ASSOC){
        $sql="SELECT * FROM media where no='$no'";
       
        $sth = $this->prepare($sql);
        $sth->execute();
//        echo $sth->errorCode();
//        echo '<br>';
//        print_r($sth->errorInfo());
        return $sth->fetchAll($fetchMode);
    }
    
    public function detail($no , $fetchMode = PDO::FETCH_ASSOC){
        $sql="SELECT d.add_date, d.update_date, u.text as uText, a.text as aText FROM detail as d left join user as a on a.no = d.add_user left join user as u on u.no = d.update_user WHERE d.no ='$no'";
        
        
        $sth = $this->prepare($sql);
        $sth->execute();
//        echo $sth->errorCode();
//        echo '<br>';
//        print_r($sth->errorInfo());
        return $sth->fetchAll($fetchMode);
    }
    
     public function select($name, $array = false, $limit=false, $source=false, $fetchMode = PDO::FETCH_ASSOC){
        
        $sql = "SELECT t.*,d.update_date FROM ". $name ." AS t LEFT JOIN detail AS d ON t.detail = d.no where d.is_delete=0 and d.is_active=1";
        
       
        if($array==true){
           
            $sql.=" and";
            if(isset($array["no"])){
                $no = $array["no"];
                $sql.=" t.no='$no' and";
                unset($array["no"]);
            }
              
            foreach ($array as $key => $value) {
                if ($source) 
                    $sql.=" $key like '%$value%' and";
                else
                    $sql.=" $key = :$key and";
            }
            
            $sql = rtrim($sql,"and");
            
        }
       
         $sql.=" order by d.order_by, d.update_date desc";
        
          if ($limit==true) {
            $sql.=" limit $limit";
        }
         
       
        $sth = $this->prepare($sql);
        if ($array==true) {
            foreach ($array as $key => $value) {
                $sth->bindValue(":$key", $value);
            }
        }
        
        
        
        $sth->execute();
//        echo $sth->errorCode();
 //       echo '<br>';
 //       print_r($sth->errorInfo());
        
       
        
        return $sth->fetchAll($fetchMode);
    }
    
   
   
    
    
    
    public function delete($tableName, $where, $limit = 1){
     
        return $this->exec("DELETE FROM $tableName WHERE $where LIMIT $limit");
    }
    
}
