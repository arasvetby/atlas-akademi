/* BLOG SLIDER */
jQuery(document).ready(function($) {
              $('.carouselBlog').owlCarousel({
                center: false,
                items: 3,
                loop: false,
                margin: 50,
                responsive:{
					0:{
						items:1
					},
					1000:{
						items:3
					}
				}
				
              });             
            });
/* BLOG SLIDER */
/* MENU DROPDOWN */                        
									$('ul.navbar-nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(250);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(550);
});
/* MENU DROPDOWN */ 
/* GALLERY */ 
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});
/* GALLERY */ 