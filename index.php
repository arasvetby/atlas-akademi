<?php

foreach (glob("libs/*.php") as $filename)
    include $filename;


session::init();

date_default_timezone_set('Europe/Istanbul');

$get = $_GET;


$cache = "cache/".md5($_SERVER['REQUEST_URI']).".ca";
if (file_exists($cache) && (time() - 1 < filemtime($cache))) {
include($cache);
exit;
}
ob_start();



if (isset($get["controller"]) && file_exists('controllers/'.$get["controller"].".php")){
    
    include 'controllers/'.$get["controller"].".php";
    if (isset($_GET["path"]) && !empty($_GET["path"])) {
        $obj = new $get["controller"]($get["path"]);
    }else{
        $obj = new $get["controller"]();
    }
   
}else{
    include 'controllers/main.php';
    if (isset($_GET["controller"])) {
        if($_GET["controller"]=="news")
            $obj = new main("news");
        else
            $obj = new main("page");
    }else{
        $obj = new main();
    }
}

if(!isset($get["controller"]) || ($get["controller"]!="panel" && $get["controller"]!="media")){
    $ch = fopen($cache, 'w');
    fwrite($ch, ob_get_contents());
    fclose($ch);
}
ob_end_flush();

