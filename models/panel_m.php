<?php

class panel_m  extends model{
    public function __construct() {
        parent::__construct();
        
    }
    
    /*Login Function*/
    public function login($username,$password){
        
        $temp = $this->db->select("user",array("username"=>$username,"password"=>$password));
        
        if (isset($temp[0])) {
            $json = json_decode($temp[0]["text"],true);
            if (isset($json["image"]) && !empty($json["image"])) {
                $ext =  $this->db->image($json["image"])[0];
                $json= array_merge($json,array("image_file"=>$ext["url"]));
            }else{
                $json= array_merge($json,array("image_file"=>""));
            }  
           
            return array_merge($json,$temp[0]);
        } else {
            return false;
        }
        
    }
    
    /*Media Function*/
    public function media($data,$type="add") {
        
        if ($type=="add") {
            $temp = array(
                "no"=>md5(uniqid()),
                "detail"=> $this->detail("media", "0", "add")
            );
            $data= array_merge($data,$temp);
            return $this->db->insert("media",$data);
        }else{
            $this->detail("media", session::get("no"), "edit", $data["detail"]);
            return $this->db->update("media",$data,$data["no"]);
        }
        

    }
    public function galery($data=false){
        
        if ($data)
            return $this->db->select("media",$data,50,"like");
        else     
            return $this->db->select("media",$data,50,"like");
    }
    
    
    /*Module Function*/
    public function table($module,$data=array()) {
        
        $temp = array();
        $image=false;
        $select=false;
        $full_image=false;
        
        if (!isset($data["no"])) {
            $temp = $this->db->table($module["table"]);
        }else{
            if (empty($data["no"])) {
                $text = array();
                foreach ($module["elements"] as $value) {
                    $text= array_merge($text,array($value["name"]=>$value["value"]));
                }
                
                $temp=array(array("no"=>"","detail"=>"","text"=> json_encode($text)));
               
            }else{
                $temp = $this->db->table($module["table"],$data["no"]);
                
            }
            unset($data["no"]);
        }
        
        $list = array();

        foreach ($module["elements"] as $value) {
            if ($value["file"]=="image") {
                $image=true;
            }else if ($value["file"]=="select"){
                $select=true;
            }
        }
        
       
        foreach ($temp as $key => $value) {

            $json = json_decode(str_replace("\'","'",$value["text"]),true);
            $status = true;
           
            
            
            foreach ($data as $k => $v) {
                if (isset($json[$k]) && strpos(mb_strtolower($json[$k]), mb_strtolower($v))===false) {
                    $status=false;
                    break;
                }
            }
           
            if ($status==true) {
                if ($image) {
                    foreach ($module["elements"] as $v) {
                        if ($v["file"]=="image") {
                            if(isset($json[$v["name"]]) && !empty($json[$v["name"]])){
                                $ext =  $this->db->image($json[$v["name"]])[0];
                                $json= array_merge($json,array($v["name"]."_file"=>$ext["url"]));
                            }else{
                                $json= array_merge($json,array($v["name"]."_file"=>""));
                            }  
                        }                     
                    }
                }
              
                if ($select) {
                  
                    foreach ($module["elements"] as $v) {
                        if ($v["file"]=="select"||$v["file"]=="type") {
                           
                            if ($v["type"]=="static") {
                                
                                foreach ($v["option"] as $o=>$opt) {
                                    if (isset($json[$v["name"]]) && !empty($json[$v["name"]]) && $o==$json[$v["name"]]) {
                                        $json= array_merge($json,array($v["name"]."-select"=>$o));
                                        $json[$v["name"]]=$opt;
                                        $json["select"]=$o;
                                    }
                                }
                            }else{
                                
                                $ext =  $this->db->select("content",array("module"=>$v["option"]));
                                $option = array();
                                foreach ($ext as $opt) {
                                    $option= array_merge($option,array($opt["no"]=>$opt["title"]));
                                    if (isset($json[$v["name"]]) && !empty($json[$v["name"]]) && $opt["no"]==$json[$v["name"]]) {
                                        $json= array_merge($json,array($v["name"]."-select"=>$opt["no"]));
                                        $json[$v["name"]]=$opt["title"];
                                    }
                                }
                               
                                $json= array_merge($json,array($v["name"]."-option"=>$option));
                            }
                        }                     
                    }
                }
                
               
                $json= array_merge($json,$value);
                array_push($list, $json);
            }
            
        }
        
        return $list;
        
    }
    public function add($module,$data) {
        
        $temp = array(
            "no"=>md5(uniqid()),
            "text"=> json_encode($data),
            "detail"=> $this->detail($module["table"], session::get("no"), "add")
        );
        
        foreach ($module["static"] as $value) {
            if(isset($data[$value]))
                $temp = array_merge($temp,array($value=>$data[$value]));
        }
        
        return $this->db->insert($module["table"],$temp);

    }
    public function edit($module,$data) {
        
        $temp = array(
            "text"=> json_encode($data),
        );
        
        foreach ($module["static"] as $value) {
            if(isset($data[$value]))
                $temp = array_merge($temp,array($value=>$data[$value]));
        }
        $this->detail($module["table"], session::get("no"), "edit", $data["detail"]);
        return $this->db->update($module["table"],$temp,$data["no"]);

    }
    
    
    /*Detail Function*/
    public function delete($table, $detail) {
        
        return $this->detail($table, session::get("no"), "delete", $detail);;
        
    }
    public function status($table, $detail,$status) {
        
        return $this->detail($table, session::get("no"), "status", $detail,$status);;
        
    }
    public function description($no) {
        $temp = array();
        
        $temp = $this->db->detail($no);
        
        $list = array();
        
        foreach ($temp as $value) {
            $add = json_decode($value["aText"],true);
            $update = json_decode($value["uText"],true);
            
            array_push($list, array("add_date"=>$value["add_date"],
                "update_date"=>$value["update_date"],
                "add_user"=>$add["username"]." (". $add["name"]." ".$add["surname"]." )",
                "update_user"=>$update["username"]." (". $update["name"]." ".$update["surname"]." )"
                ));
                
        }
      
        return $list;
    }
    public function order($table, $detail, $order) {
        
        return $this->detail($table, session::get("no"), "order", $detail,$order);;
        
    }
    public function detail($tablo, $user, $type, $no="", $status=true) {
        
        if ($type=="add") {
            $temp = array(
                "no"=>md5(uniqid()),
                "tablo_name"=>$tablo,
                "add_date"=> date('Y-m-d H:i:s'),
                "add_user"=> $user,
                "order_by"=>$this->db->identy("detail")["id"]+1
            );
            
            $this->db->insert("detail",$temp);
            return $temp["no"];
        }else if ($type=="edit"){
            $temp = array(
               
                "update_date"=> date('Y-m-d H:i:s'),
                "update_user"=> $user,
                
            );
            
            
            return $this->db->update("detail",$temp,$no);
        }else if ($type=="status"){
            $temp = array(
               
                "update_date"=> date('Y-m-d H:i:s'),
                "update_user"=> $user,
                "is_active"=>$status
            );
            
            
            return $this->db->update("detail",$temp,$no);
        }else if ($type=="order"){
            $temp = array(
               
                "update_date"=> date('Y-m-d H:i:s'),
                "update_user"=> $user,
                "order_by"=>$status
            );
            
            
            return $this->db->update("detail",$temp,$no);
        }else{
            $temp = array(
               
                "delete_date"=> date('Y-m-d H:i:s'),
                "delete_user"=> $user,
                "is_delete"=>true
            );
            
            
            return $this->db->update("detail",$temp,$no);
        }
        
    }
}
