<?php

class main_m  extends model{
    public function __construct() {
        parent::__construct();
        
    }
    
            
    public function select($module,$data,$type="list",$limit=false,$modules=false){
        
        if($module=="content"){
            $temp = $this->db->select($module,$data,$limit);
            if(count($temp)>0)
                $module = $modules[$temp[0]["module"]];
        }
        else
            $temp = $this->db->select($module["table"],$data,$limit);
        $list = array();
        
        $image=false;
        $select=false;
        
        foreach ($module["elements"] as $value) {
            if ($value["file"]=="image" || $value["file"]=="full-image") {
                $image=true;
                
            }else if ($value["file"]=="select"){
                $select=true;
            }
        }
        
       
        foreach ($temp as $key => $value) {
            $json = json_decode(str_replace("\'","'",$value["text"]),true);
            
            if ($image) {
                foreach ($module["elements"] as $v) {
                    if ($v["file"]=="image" || $v["file"]=="full-image") {
                        if(isset($json[$v["name"]]) && !empty($json[$v["name"]])){
                            $ext =  $this->db->image($json[$v["name"]])[0];
                            $json= array_merge($json,array($v["name"]."_file"=>$ext["url"]));
                        }else{
                            $json= array_merge($json,array($v["name"]."_file"=>""));
                        }  
                    }
                }
            }
            if ($select) {

                foreach ($module["elements"] as $v) {
                    if ($v["file"]=="select" || $v["file"]=="type") {

                        if ($v["type"]=="static") {

                            foreach ($v["option"] as $o=>$opt) {
                                if (isset($json[$v["name"]]) && !empty($json[$v["name"]]) && $o==$json[$v["name"]]) {
                                    $json= array_merge($json,array($v["name"]."-select"=>$o));
                                    $json[$v["name"]]=$opt;
                                }
                            }
                        }else{

                            $ext =  $this->db->select("content",array("module"=>$v["option"]));
                            $option = array();
                            $sluq="";
                            foreach ($ext as $opt) {
                                $option= array_merge($option,array($opt["no"]=>$opt["title"]));
                                if (isset($json[$v["name"]]) && !empty($json[$v["name"]]) && $opt["no"]==$json[$v["name"]]) {
                                    $json= array_merge($json,array($v["name"]."-select"=>$opt["no"]));
                                    $json[$v["name"]]=$opt["title"];
                                    $sluq=$opt["sluq"];
                                }
                            }

                            $json= array_merge($json,array($v["name"]."-option"=>$option,$v["name"]."-sluq"=>$sluq));
                        }
                    }                     
                }
            }

            if(isset($value["title"]))
                $value["title"]=str_replace("\'","'",$value["title"]);

            $json= array_merge($json,$value);
            array_push($list, $json);
            
            
        }
        if ($type=="list") 
            return $list;
        else
            return (isset($list[0])) ? $list[0] : null;
        
    }        
    
    
}