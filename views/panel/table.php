
     <section class="content-header">
      <h1>
        {{title}}
        <small>{{sub_title}}</small>
      </h1>
      <ol class="breadcrumb">
    <li><a href="#"><i class="fas fa-tachometer-alt"></i> Anasayfa</a></li>
    <li class="active">{{sub_title}}</li>
  </ol>
    </section>
    
    <section class="content">
        
        <div class="row">
           
            
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <a onclick="GetPage('{{url}}?controller=panel&path=back','main')" class="btn btn-basic"><i class="fa fa-arrow-left"></i> Geri</a>
                        <a onclick="GetPage('{{url}}?controller=panel&path={{add}}&module={{module}}','{{id}}')" class="btn btn-success" ><i class="fas fa-plus"></i> Yeni {{name}} Ekle</a>
                    </div>
                    <div class="box-body">
                
                        {{filter}}
                        
                        <div id="table">
                            {{table}}
                        </div>
                            
                     
                    </div>
                </div>
            </div>
        </div>
      
        
     
        
        
    </section>
    
    
    <div id="table-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">{{name}} Ekle/Güncele</h4>
            </div>
            <div class="modal-body" id="modal">
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Geri</button>
              <a onclick="SavePage('{{url}}?controller=panel&path=add&module={{module}}','modal-{{module}}')" class="btn btn-primary" data-dismiss="modal">Kaydet</a>
            </div>
          </div>

        </div>
    </div>
    
    <div id="detail-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">{{name}} Detayları</h4>
            </div>
            <div class="modal-body" id="detail">
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Geri</button>
             
            </div>
          </div>

        </div>
    </div>
   <script>
   $('#disabledButton').click(false);
    function imageError(element) {
            element.onerror = '';
            element.src = '{{media}}no-image.png';
    }
    $(function() {
    $('[data-spzoom]').spzoom();
});
    </script>