<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{title}}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{style}}bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{style}}bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{style}}bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{style}}dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{style}}dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{style}}bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{style}}bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{style}}bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{style}}bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{style}}plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

   <link href="{{style}}css/toggle.css" rel="stylesheet" type="text/css"/>
   <link href="{{style}}css/adminCustom.css" rel="stylesheet" type="text/css"/>
   
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    
<!-- jQuery 3 -->
<script src="{{style}}bower_components/jquery/dist/jquery.min.js"></script>


<!-- jQuery UI 1.11.4 -->
<script src="{{style}}bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{style}}bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="{{style}}bower_components/raphael/raphael.min.js"></script>
<script src="{{style}}bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="{{style}}bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="{{style}}plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{style}}plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{style}}bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="{{style}}bower_components/moment/min/moment.min.js"></script>
<script src="{{style}}bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="{{style}}bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{style}}plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="{{style}}bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{style}}bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{style}}dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{style}}dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{style}}dist/js/demo.js"></script>
<!-- CK Edıtor -->
<script src="{{style}}bower_components/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{style}}bower_components/ckeditor/styles.js?t=I63C"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="{{style}}js/bootstrap-notify.min.js"></script>







<script src="{{style}}dist/js/demo.js"></script>
<script src="{{style}}js/ajax.js"></script>
<script src="{{style}}js/number-input.js"></script>
<script src="{{style}}js/jquery.hideseek.js"></script>


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div>


<!-- Main content -->
<section class="content container-fluid">
   <div class="box medyaStyle">
      <div class="box-header">
         <ul class="nav nav-tabs">
            <li>
               <div class="file-upload">
                  <form id="upload-media" method="post" enctype="multipart/form-data">
                     <label for="upload" class="file-upload__label">Dosya Yükle</label>
                     <input id="upload" onchange="PostMedia('{{url}}?controller=media&path=load','media-main')" class="file-upload__input" type="file" name="files[]" multiple="multiple">
                  </form>
               </div>
            </li>
            <li class="active"><a data-toggle="tab" href="#tab-ortamKutuphanesi">Ortam Kütüphanesi</a></li>
         </ul>
      </div>
      <div class="box-body">
         <div class="tab-content">
            <div id="tab-ortamKutuphanesi" class="tab-pane fade in active">
               <div class="row mediaJS" style="margin-right: 0;">
                  <div class="col-md-9 modalImageGallery" id="imageGallery">
                     <div class="row mediaFilters">
                        
                        <input type="text" class="form-control pull-right" placeholder="Ortam Dosyalarında Ara" id="media-name" name="media-name" autocomplete="off">
                           
                    </div>
                        <script>
                              $(document).ready(function(){
                                  $("#media-name").keyup(function(){
                                      console.log($(this).val());
                                      GetPage('{{url}}?controller=media&path=source&media-name='+$(this).val(),'media-main');
                                  });
                                      
                              });
                        </script>           
                     <div class="row imageGalleryPadding" id="media-main">

                      {{media-main}}
                      
                     </div>
                  </div>
                  <div class="col-md-3 modalImageDetails" id="imageDetails">
                     <div class="imageDetailsHide">
                        <div class="imageDetailsBorder">
                           <div class="row">
                              <div class="col-md-12">
                                 <p>MEDYA DETAYLARI</p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="">
                                    <img src="" id="secilenResim"
                                       class="img-responsive mCenter" max-height="100">
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <h1 id="baslik">YAZICI.jpg</h1>
                                 <h3 id="tarih">22 Mart 2018</h3>
                                 <h3 id="boyut">29 KB</h3>
                                 <h3 id="resimBoyut">253 × 253</h3>
                                 <h3><a id="resimSil" onclick="" style="cursor:pointer;">Kalıcı olarak sil</a></h3>
                              </div>
                           </div>
                        </div>
                        <form id="media-edit" method="post">
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">URL</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="url" disabled>
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Başlık</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="name">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Title</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="title">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Alt</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="alt">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Boyut</label>
                              <input type="text" class="form-control sizeInputs" style="margin-left: 15px;" name="width">
                              <span><i class="fas fa-times"></i></span>
                              <input type="text" class="form-control sizeInputs" name="height">
                           </div>
                           <input type="hidden" name="no" />
                           <input type="hidden" name="detail" />
                           <div class="clearfix"></div>
                           <div class="form-group">
                              <input onclick="SavePage('{{url}}?controller=media&path=edit','media-edit','media-main')" value="Kaydet" class="btn btn-success" />
                           </div>
                           <div class="form-group">
                               <input onclick="returnFileUrl()" value="Sayfaya Ekle" data-dismiss="modal" class="btn btn-primary " />
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>    
   </div>
</section>
</div>

<!-- ./wrapper -->
<script>
        // Helper function to get parameters from the query string.
        function getUrlParam(paramName) {
            var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
            var match = window.location.search.match(reParam);

            return (match && match.length > 1) ? match[1] : null;
        }
        // Simulate user action of selecting a file to be returned to CKEditor.
        function returnFileUrl() {

            var funcNum = getUrlParam('CKEditorFuncNum');
            var fileUrl = $("#imageDetails input[name='url']").val();
            window.opener.CKEDITOR.tools.callFunction(funcNum, fileUrl);
            window.close();
        }
    </script>


</body>
</html>