</div> 

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright 2018 YEDİTEPE ATLAS AKADEMİ</strong> | Tüm Hakları Saklıdır.
  </footer>

  
</div>

<div id="image-modal" class="modal fade medyaStyle" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Ortam Ekle</h4>
            <ul class="nav nav-tabs">
               <li>
                  <div class="file-upload">
                    <form id="upload-media" method="post" enctype="multipart/form-data">
                     <label for="upload" class="file-upload__label">Dosya Yükle</label>
                     <input id="upload" onchange="MediaPost()" class="file-upload__input" type="file" name="files[]" multiple="multiple">
                    </form>
                  </div>
               </li>
               <li class="active"><a data-toggle="tab" href="#tab-ortamKutuphanesi">Ortam Kütüphanesi</a></li>
            </ul>
         </div>
         <div class="modal-body">
            <div class="tab-content">
                 <div id="tab-ortamKutuphanesi" class="tab-pane fade in active">
               <div class="row mediaJS" style="margin-right: 0;">
                  <div class="col-md-9 modalImageGallery" id="imageGallery">
                     <div class="row mediaFilters">
                        
                        
                      <input type="text" class="form-control pull-right" placeholder="Ortam Dosyalarında Ara" id="media-name" name="media-name" autocomplete="off">
                           
                    </div>
                           
               <div id="media-main" class="row imageGalleryPadding">
                    
               </div>
                       </div>
                  <div class="col-md-3 modalImageDetails" id="imageDetails">
                     <div class="imageDetailsHide">
                        <div class="imageDetailsBorder">
                           <div class="row">
                              <div class="col-md-12">
                                 <p>MEDYA DETAYLARI</p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="">
                                    <img src="" id="secilenResim"
                                       class="img-responsive mCenter" max-height="100">
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <h1 id="baslik">YAZICI.jpg</h1>
                                 <h3 id="tarih">22 Mart 2018</h3>
                                 <h3 id="boyut">29 KB</h3>
                                 <h3 id="resimBoyut">253 × 253</h3>
                                 <h3><a id="resimSil" onclick="" style="cursor:pointer;">Kalıcı olarak sil</a></h3>
                              </div>
                           </div>
                        </div>
                        <form id="media-edit" method="post">
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">URL</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="url" disabled>
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Başlık</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="name">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Title</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="title">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Alt</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="alt">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Boyut</label>
                              <input type="text" class="form-control sizeInputs" style="margin-left: 15px;" name="width">
                              <span><i class="fas fa-times"></i></span>
                              <input type="text" class="form-control sizeInputs" name="height">
                           </div>
                           <input type="hidden" name="no" />
                           <input type="hidden" name="detail" />
                           <div class="clearfix"></div>
                           <div class="form-group">
                              <input type="hidden" id="select-file-input" value="image" >
                              <input onclick="MediaSave()" value="Kaydet" class="btn btn-success" />
                           </div>
                        </form>
                     </div>
                  </div>
            </div>
         </div>
         <div class="modal-footer">
            
            <input onclick="SelectMedia()" value="Sayfaya Ekle" data-dismiss="modal" class="btn btn-primary " />
            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
         </div>
      </div>
   </div>
</div>

<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{style}}bower_components/jquery/dist/jquery.min.js"></script>


<!-- jQuery UI 1.11.4 -->
<script src="{{style}}bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{style}}bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="{{style}}bower_components/raphael/raphael.min.js"></script>
<script src="{{style}}bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="{{style}}bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="{{style}}plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{style}}plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{style}}bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="{{style}}bower_components/moment/min/moment.min.js"></script>

<script src="{{style}}bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="{{style}}plugins/input-mask/jquery.inputmask.js"></script>
<script src="{{style}}plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="{{style}}plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="{{style}}bower_components/moment/min/moment.min.js"></script>
<script src="{{style}}bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="{{style}}bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="{{style}}bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="{{style}}plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="{{style}}bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="{{style}}plugins/iCheck/icheck.min.js"></script>

<script src="{{style}}bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="{{style}}plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="{{style}}bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{style}}bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{style}}dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{style}}dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{style}}dist/js/demo.js"></script>
<!-- CK Edıtor -->
<script src="{{style}}bower_components/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{{style}}bower_components/ckeditor/styles.js?t=I63C"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="{{style}}js/bootstrap-notify.js"></script>







<script src="{{style}}dist/js/demo.js"></script>
<script src="{{style}}js/ajax.js"></script>
<script src="{{style}}js/number-input.js"></script>
<script src="{{style}}js/jquery.hideseek.js"></script>
<script src="{{style}}js/bootstrap-datepicker.tr.js"></script>
<script src="{{style}}plugins/spzoom-master/jquery.spzoom.js"></script>



<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

 <script>

 
    $(document).ready(function(){
   
        $("#media-name").keyup(function(){
       
        GetPage('{{url}}?controller=media&path=source&select='+$("#select-file-input").val()+'&media-name='+$(this).val(),'media-main');
        });
                                      
    });

    function MediaSave(){
      SavePage('{{url}}?controller=media&path=edit&select='+$("#select-file-input").val(),'media-edit','media-main')
    }
    function MediaPost(){
      PostMedia('{{url}}?controller=media&path=load&select='+$("#select-file-input").val(),'media-main');
    } 
 </script>
 <script>
   var $loading = $('.vertical-centered-box').hide();
   //Attach the event handler to any element
   $(document)
      .ajaxStart(function () {
      //ajax request went so show the loading image
      $loading.show();
   })
     .ajaxStop(function () {
      //got response so hide the loading image
      $loading.hide();
   });
   </script>
</body>
</html>
