<table class="table table-responsive table-hover table-striped">
    
    <tr>
        <th>Ekleyen Kullanıcı</th>
        <td>{{add_user}}</td>
    </tr>
     <tr>
        <th>Eklenme Tarihi</th>
        <td>{{add_date}}</td>
    </tr>
     <tr>
        <th>Son Güncelleyen Kullanıcı</th>
        <td>{{update_user}}</td>
    </tr>
    <tr>
        <th>Son Güncellenme Tarihi</th>
        <td>{{update_date}}</td>
    </tr>
    
</table>