<div class="form-group" id="div-{{id}}">
    <label for="{{name}}">{{label}}</label>

    <div class="input-group date">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
      <input type="text" class="form-control pull-right" id="{{id}}" name="{{name}}" value="{{value}}">
    </div>
    <!-- /.input group -->
  </div>

<script>
$(document).ready(function(){
    var date = new Date();
    date.setDate(date.getDate());
    $('#{{name}}').datepicker({
        format: 'dd/mm/yyyy',
        language: 'tr',
        autoclose: true
        //startDate: date
    });
    
});
</script>