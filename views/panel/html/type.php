<div class="form-group" id="div-{{id}}">
    <label for="{{name}}">{{label}}</label>
    <select class="form-control {{class}}" id="{{id}}" placeholder="{{placeholder}}" name="{{name}}" {{multiple}}>
        <option value="">{{label}}</option>  
      {{option}}
    </select>
</div>

<script>
(function ($) {
	  $.each(['show', 'hide'], function (i, ev) {
	    var el = $.fn[ev];
	    $.fn[ev] = function () {
	      this.trigger(ev);
	      return el.apply(this, arguments);
	    };
	  });
	})(jQuery);
$(document).ready(function(){
    var effect_{{name}} = {{effect}};
  
   
    if({{reset}}==1){
        $("#div-"+effect_{{name}}["{{select}}"]).show();
        $("#{{id}}").val("");
        $('#div-{{id}}').on('hide', function() {
            $.each(effect_{{name}},function (x,y) {
                $("#div-"+y).hide();
            });
        });
    }
        
        
    $.each(effect_{{name}},function (x,y) {
        $("#div-"+y).hide();
    });

    
   
    
    $("#{{id}}").change(function(){
        var value = $(this).val();
        $.each(effect_{{name}},function (x,y) {
            $("#div-"+y).hide();
        });

        $("#div-"+effect_{{name}}[value]).show();

    });

   
    if({{reset}}==0){
        $("#div-"+effect_{{name}}["{{select}}"]).show();
    }
    
});
</script>