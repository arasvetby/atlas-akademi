<div id="div-{{id}}">
    <div class="form-group" >
        <label> Galeri Başlığı </label>
        <input type="text" class="form-control " id="gallery-title" placeholder="Galeri Başlığı" value="">
    </div>
    
    <div class="form-group" >
        <label >İçerik Türü Seçiniz</label> <small>( Resim, Video veya Dosya )</small>
        <select class="form-control" id="gallery-type" >
            <option value="0">İçerik Türü Seçiniz</option>  
            <option value="1">Resim Türünde</option>
            <option value="2">Video Türünde</option>
            <option value="3">Dosya Türünde</option>
        </select>
    </div>


    <div class="form-group" id="div-gallery-image">
        <label>Galeri İçeriği</label>
        <div class="input-group">
            <span class="input-group-btn">
                <button class="btn btn-primary" onclick="GetPage('{{url}}?controller=media&path=source&select=gallery-image','media-main')" data-toggle="modal" data-target="#image-modal" type="button">Medya Seç</button>
            </span>
            <input id="file-gallery-image" type="text" url="" name="gallery-image" value="" placeholder="Galeri İçeriği" class="form-control" autocomplete="off" readonly="">
        </div>
    </div>

    <div class="form-group" id="div-gallery-file">
        <label>Galeri İçeriği</label>
        <div class="input-group">
            <span class="input-group-btn">
                <button class="btn btn-primary" onclick="GetPage('{{url}}?controller=media&path=source&select=gallery-file','media-main')" data-toggle="modal" data-target="#image-modal" type="button">Dosya Seç</button>
            </span>
            <input id="file-gallery-file" type="text" url="" name="gallery-file" value="" placeholder="Galeri İçeriği" class="form-control" autocomplete="off" readonly="">
            
        </div>
    </div>
    <div class="form-group" id="div-gallery-video">
        <label> Galeri İçeriği </label> <small>Video Linkini Kopyalayın</small>
        <input type="text" class="form-control " id="gallery-video" placeholder="Galeri İçeriği" value="">
    </div>

    <div class="form-group">
        <label>Galeri Kısa Açıklaması</label> 
        <textarea rows="5" class="form-control " id="gallery-content" placeholder="Galeri Kısa Açıklaması"></textarea>
    </div>
    <div class="form-group">
        <a class="btn btn-primary" onclick="GalleryAdd()">Galeriye Ekle </a>
    </div>

    <div class="form-group" id="gallery-list">

    </div>
    <input type="hidden" name="{{name}}" value='{{value}}' id="gallery-value">
</div>

<script>

var count = 0;
var temp ="item-0";
function GalleryAdd(){

    var title = $("#gallery-title").val();
    var type = $("#gallery-type").val();
    var no = "";
    var url = "";
    var src = "";
    var content = $("#gallery-content").val();
    if(type==0){
        $.notify({
            icon: 'glyphicon glyphicon-ok-sign',
            message: "Galeri Türü Seçimi Boş Bırakılamaz!" 
        },{
            type: 'danger'
        });
        return 0;
    }else if(type==1){
        no = $("#file-gallery-image").val();
        url = $("#file-gallery-image").attr("url");
        src = $("#file-gallery-image").attr("url");
    }else if (type==2){
        url = $("#file-gallery-video").val();
        src = "media/no-image.png";
    }else{
        no = $("#file-gallery-file").val();
        url = $("#file-gallery-file").attr("url");
        src = "media/no-image.png";
    }
    
    if ($("#"+temp).length ==0) {
        count++;
        var eleman = '<div id="item-'+count+'" class="col-sm-3 col-xs-6 gallery-item" data-no="'+no+'" data-title="'+title+'" data-content="'+content+'" data-type="'+type+'" data-src="'+src+'">'+
                 '<div class="thumbnail">'+
                 '<div class="frow">'+
                 '<div class="col-md-6" style="padding-left:0;"><a style="margin-bottom:10px;" onclick="GalleryEdit(\'item-'+count+'\')" class="btn btn-success btn-block">Düzenle</a></div>'+
                 '<div class="col-md-6" style="padding-right:0;"><a onclick="GalleryDelete(\'item-'+count+'\')" class="btn btn-danger btn-block">Sil</a></div>'+
                 '</div>'+
				 '<div class="secrow" style="padding:5px;">'+
				 '<img src="'+src+'" title="'+title+'" class="img-responsive">'+
				 '</div>'+
		   		 '</div>'+
			     '</div>';

        
        $("#gallery-list").append(eleman);

        
    }else{
        $("#"+temp).attr("data-title",title);
        $("#"+temp).attr("data-type",type);
        $("#"+temp).attr("data-content",content);
        $("#"+temp).attr("data-src",src);
        $("#"+temp).attr("data-no",no);
        $("#"+temp+" img").attr("src",src);
        $("#"+temp+" img").attr("title",title);
    }

   
    temp="item-0";
    $("#gallery-title").val("");
    $("#gallery-type").val("0");
    $("#gallery-content").val("");
    $("#file-gallery-image").val("");
    $("#file-gallery-image").attr("url","");
    $("#gallery-type").change();
    GalleryValue();
}

function GalleryEdit(x){
    $("#gallery-title").val($("#"+x).attr("data-title"));
    $("#gallery-type").val($("#"+x).attr("data-type"));
    $("#gallery-content").val($("#"+x).attr("data-content"));
    $("#file-gallery-image").val($("#"+x).attr("data-no"));
    $("#file-gallery-image").attr("url",$("#"+x).attr("data-src"));
    
    temp=x;
    $("#gallery-type").change();
}

function GalleryDelete(x){
    $("#"+x).remove();
    $("#gallery-list").sortable();
    GalleryValue();
    count--;
}
function GalleryValue(){
    var value ="["
    $.each($(".gallery-item"),function(x,y){
        if(typeof $(y).attr("data-title")!=="undefined" && typeof $(y).attr("data-content")!=="undefined" && typeof $(y).attr("data-src")!=="undefined" && typeof $(y).attr("data-type")!=="undefined" && typeof $(y).attr("data-no")!=="undefined")
            value += "{\"title\":\""+$(y).attr("data-title")+"\",\"content\":\""+$(y).attr("data-content")+"\",\"src\":\""+$(y).attr("data-src")+"\",\"type\":\""+$(y).attr("data-type")+"\",\"no\":\""+$(y).attr("data-no")+"\"},";
               
    });
    value = value.substring(0, value.length - 1)+"]";
    $("#gallery-value").val(value);
}
$(document).ready(function(){
    if($("#gallery-value").val()==="")
        $("#gallery-value").val("[]");
    $.each(JSON.parse($("#gallery-value").val()),function(x,y){
        count++;
        var eleman = '<div id="item-'+count+'" class="col-sm-3 col-xs-6 gallery-item" data-no="'+y["no"]+'" data-title="'+y["title"]+'" data-content="'+y["content"]+'" data-type="'+y["type"]+'" data-src="'+y["src"]+'">'+
                 '<div class="thumbnail">'+
                 '<div class="frow">'+
                 '<div class="col-md-6" style="padding-left:0;"><a style="margin-bottom:10px;" onclick="GalleryEdit(\'item-'+count+'\')" class="btn btn-success btn-block">Düzenle</a></div>'+
                 '<div class="col-md-6" style="padding-right:0;"><a onclick="GalleryDelete(\'item-'+count+'\')" class="btn btn-danger btn-block">Sil</a></div>'+
                 '</div>'+
				 '<div class="secrow" style="padding:5px;">'+
				 '<img src="'+y["src"]+'" title="'+y["title"]+'" class="img-responsive">'+
				 '</div>'+
		   		 '</div>'+
			     '</div>';

        
        $("#gallery-list").append(eleman);
        
    });


    var effect_{{name}} = {'1':'gallery-image','2':'gallery-video','3':'gallery-file'};
  
    $("#gallery-list").sortable({
       
        beforeStop: function( event, ui ) {GalleryValue();}
    });
    $("#gallery-list").disableSelection();
      
    $.each(effect_{{name}},function (x,y) {
        $("#div-"+y).hide();
    });
 
    $("#gallery-type").change(function(){
        var value = $(this).val();
        $.each(effect_{{name}},function (x,y) {
            $("#div-"+y).hide();
        });

        $("#div-"+effect_{{name}}[value]).show();

    });
});
</script>