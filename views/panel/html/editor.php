<div class="form-group" id="div-{{id}}">
    <label for="{{name}}">{{label}}</label>
    <textarea name="{{name}}" class="form-control {{class}}" id="{{id}}" placeholder="{{placeholder}}">{{value}}</textarea>
</div>
<script type="text/javascript">
   $(function () {
       CKEDITOR.replace( '{{id}}', {
           language: 'tr',
           uiColor: '#9AB8F3',
           height: 500,
           filebrowserBrowseUrl:  '{{url}}?controller=media&path=ckmedia',
           filebrowserWindowWidth: '90%',
           filebrowserWindowHeight: '80%'
       });
   });
   
</script>