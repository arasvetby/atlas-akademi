<form id="filter-{{module}}">
    <table class="table">
        <tr>
            
                {{filter-elements}}
           
            <td>
                <a onclick="SourcePage('{{url}}?controller=panel&path=source&module={{module}}','filter-{{module}}')" class="btn btn-primary  filterButtons filterSearch"><i class="fa fa-search" ></i> Ara</a>     
                <a onclick="SourcePage('{{url}}?controller=panel&path=source&module={{module}}','filter-{{module}}','Reset')" class="btn btn-primary  filterButtons"><i class="fa fa-refresh" ></i> Temizle</a>
            </td>
        </tr>
    </table>
</form>
<script>
$(document).ready(function() {
  $("#filter-{{module}}").keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      SourcePage('{{url}}?controller=panel&path=source&module={{module}}','filter-{{module}}');
      return false;
    }
  });
});
</script>