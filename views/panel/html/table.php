<table id="table-{{module}}" class="table table-striped table-bordered" style="width:100%">
<thead>                   
    <tr>
        {{table-title}}
    </tr>
</thead>
<tbody>
    {{table-data}}
</tbody>
<tfoot>
    <tr>
        {{table-title}}
    </tr>
</tfoot>
</table>
<script>
  
$(document).ready(function() {
    $('#table-{{module}}').DataTable({
        "searching": false,
         "responsive": true,
        "columnDefs": [
            { "width": "105px", "targets": 0 }
          ],        
         "language": {
            "lengthMenu": "Sayfada _MENU_ Adet Sonuç Göster",
            "zeroRecords": "Kayıtlı İçerik Bulunmamaktadır. Sol Üste Bulunan Yeni {{name}} Ekle Seçeneğini Kullanarak Yeni {{name}} Ekleyebilirsiniz.",
            "info": "Toplam _PAGES_ Sayfadan, Sayfa Numarası _PAGE_ Olan Gösteriliyor ",
            "infoEmpty": "Aramanız İle İlgili Sonuç Bulunamamıştır.",
            "infoFiltered": "(Toplam _MAX_ Satır Data Filtrelendi)",
            "sSearch":        "Arama",
            "oPaginate": {
               "sFirst":      "ilk Sayfa",
               "sPrevious":   "Önceki",
               "sNext":       "Sonraki",
               "sLast":       "Son Sayfa"
            }        
        }  
        
    });
} );

  
   
  

</script>