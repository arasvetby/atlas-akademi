<div class="form-group" id="div-{{id}}">
    <label for="{{name}}"> {{label}} </label><small>{{small}}</small>
    <input type="{{type}}" class="form-control {{class}}" name="{{name}}" id="{{id}}" placeholder="{{placeholder}}" value="{{value}}">
</div>
