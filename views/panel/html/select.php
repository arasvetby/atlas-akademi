<div class="form-group {{class}}" id="div-{{id}}" >
    <label for="{{name}}">{{label}}</label>
    <select class="form-control " placeholder="{{placeholder}}" id="{{id}}" name="{{name}}" {{multiple}}>
        <option value="">{{label}}</option>  
      {{option}}
    </select>
</div>

<script>
$(document).ready(function(){$('.select2').select2();});
</script>