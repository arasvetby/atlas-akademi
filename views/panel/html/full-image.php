
<div id="div-{{id}}">
    <div class="form-group" >
        <label for="text-{{name}}"> {{label}} Başlığı </label>
        <input type="text" class="form-control" name="text-{{name}}" id="text-{{id}}" placeholder="{{placeholder}} Başlığı" value="{{text_value}}">
    </div>

    <div class="form-group" >
        <label for="{{name}}">{{label}}</label><small>{{small}}</small>
    <div class="input-group">
        <span class="input-group-btn">
            <button class="btn btn-primary" onclick="GetPage('{{url}}?controller=media&path=source&select={{name}}','media-main')" data-toggle="modal" data-target="#image-modal" type="button">Resim Seç</button>
        </span>
        <input id="file-{{id}}" type="text" name="{{name}}" value="{{value}}" placeholder="{{placeholder}}" class="form-control {{class}}" autocomplete="off" readonly="">
    </div>
    </div>

    <div class="form-group " >
        <label for="textarea-{{name}}">{{label}}</label> 
        <textarea rows="5" name="textarea-{{name}}" class="form-control " id="textarea-{{id}}" placeholder="{{placeholder}}">{{textarea_value}}</textarea>
    </div>
</div>