<div class="form-group {{class}}" id="div-{{id}}">
    <label for="{{name}}">{{label}}</label> <small> {{small}}</small>
    <textarea rows="5" name="{{name}}" class="form-control " id="{{id}}" placeholder="{{placeholder}}">{{value}}</textarea>
</div>
