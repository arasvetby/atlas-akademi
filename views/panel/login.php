<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{title}}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{style}}bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{style}}bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{style}}bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{style}}dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{style}}plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>{{title}}</b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Oturum açmak için giriş yapın.</p>
   
    <form action="{{url}}?controller=panel&path=dashboard" method="post">
      <input type="hidden" name="status" value="1">  
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Kullanıcı Adı">
        
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Şifre">
        
      </div>
      <div class="row">
        <div class="col-xs-8">
<!--          <div class="checkbox icheck">
            <label>
              <input type="checkbox" id="remember" value="1" name="remember"> Beni Hatırla
            </label>
          </div>-->
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Giriş Yap</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

   

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{style}}bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{style}}bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="{{style}}plugins/iCheck/icheck.min.js"></script>
<script src="{{style}}js/bootstrap-notify.js"></script>
<script>
  $(function () {
    $('#remember').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
    
    if ({{message}}===1) {
        $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                message: "Kullanıcı Adı veya Şifre Hatalı! Tekrar Deneyiniz."
            },{
                type: 'danger'
            });
    }else  if ({{message}}===2) {
        $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                message: "Kullanıcı Adı veya Şifre Boş Bırakılamaz!"
            },{
                type: 'warning'
            });
    }else if ({{message}}==3){
        $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                message: "Giriş Başarılı!"
            },{
                type: 'success'
            });
            
          
        
        window.location = "{{url}}?controller=panel&path=dashboard";
        

        
    }
    
  });
</script>
</body>
</html>
