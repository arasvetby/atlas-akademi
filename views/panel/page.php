<section class="content-header">
  <h1>
    {{name}} Ekle/Düzenle
    <small>{{sub_title}}</small>
  </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fas fa-tachometer-alt"></i> Anasayfa</a></li>
    <li class="active">{{sub_title}}</li>
  </ol>
</section>

<section class="content">

    <form id="page-{{module}}">
        <div class="{{left_col}}">
            <div class="box">
                <div class="box-body">
                    {{page-elements-left}}
                </div>
            </div>
        </div>
        <div class="{{right_col}}">
            <div class="box">
                <div class="box-body">
                    {{page-elements-right}}
                    
                    <div class="form-group">
                        <input type="reset" class="btn btn-danger" value="Temizle">
                        <a onclick="SavePage('{{url}}?controller=panel&path=add&module={{module}}','page-{{module}}','main')" class="btn btn-primary ">Kaydet</a>
                    </div>
                </div>
            </div>
        </div>
       

    </form>
        
</section>