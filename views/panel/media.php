<!-- Content Header (Page header) -->

<!-- Main content -->
<section class="content container-fluid">
   <div class="box medyaStyle">
      <div class="box-header">
         <ul class="nav nav-tabs">
            <li>
               <div class="file-upload">
                  <form id="upload-media" method="post" enctype="multipart/form-data">
                     <label for="upload" class="file-upload__label">Dosya Yükle</label>
                     <input id="upload" onchange="PostMedia('{{url}}?controller=media&path=load','media-main')" class="file-upload__input" type="file" name="files[]" multiple="multiple">
                  </form>
               </div>
            </li>
            <li class="active"><a data-toggle="tab" href="#tab-ortamKutuphanesi">Ortam Kütüphanesi</a></li>
         </ul>
      </div>
      <div class="box-body">
         <div class="tab-content">
            <div id="tab-ortamKutuphanesi" class="tab-pane fade in active">
               <div class="row mediaJS" style="margin-right: 0;">
                  <div class="col-md-9 modalImageGallery" id="imageGallery">
                     <div class="row mediaFilters">
                        <input type="text" class="form-control pull-right" placeholder="Ortam Dosyalarında Ara" id="media-name" name="media-name" autocomplete="off">
                           
                     </div>
                      <script>
                        $(document).ready(function(){
                            $("#media-name").keyup(function(){
                                console.log($(this).val());
                                GetPage('{{url}}?controller=media&path=source&media-name='+$(this).val(),'media-main');
                            });
                                
                        });
                        </script>
                      
                     <div class="row imageGalleryPadding" id="media-main">

                      {{media-main}}
                      
                     </div>
                  </div>
                  <div class="col-md-3 modalImageDetails" id="imageDetails">
                     <div class="imageDetailsHide">
                        <div class="imageDetailsBorder">
                           <div class="row">
                              <div class="col-md-12">
                                 <p>MEDYA DETAYLARI</p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="">
                                    <img src="" id="secilenResim"
                                       class="img-responsive mCenter" max-height="100">
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <h1 id="baslik">YAZICI.jpg</h1>
                                 <h3 id="tarih">22 Mart 2018</h3>
                                 <h3 id="boyut">29 KB</h3>
                                 <h3 id="resimBoyut">253 × 253</h3>
                                 <h3><a id="resimSil" onclick="" style="cursor:pointer;">Kalıcı olarak sil</a></h3>
                              </div>
                           </div>
                        </div>
                        <form id="media-edit" method="post">
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">URL</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="url" disabled>
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Başlık</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="name">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Title</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="title">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Alt</label>
                              <div class="col-md-8">
                                 <input type="text" class="form-control" name="alt">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label class="col-md-4 col-form-label text-right">Boyut</label>
                              <input type="text" class="form-control sizeInputs" style="margin-left: 15px;" name="width">
                              <span><i class="fas fa-times"></i></span>
                              <input type="text" class="form-control sizeInputs" name="height">
                           </div>
                           <input type="hidden" name="no" />
                           <input type="hidden" name="detail" />
                           <div class="clearfix"></div>
                           <div class="form-group">
                              
                              <input onclick="MediaSave()" value="Kaydet" class="btn btn-success" />
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>    
   </div>
</section>
