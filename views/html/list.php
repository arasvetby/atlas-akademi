
<!-- Page Header -->

	  <div class="container-fluid customPageHeader">
		<div class="container">
		<h3><?php echo $category["title"]; ?></h3>
				<div class="breadCrumb">
					<ul>
					<li><a href="<?php echo url; ?>">Anasayfa</a></li>
						<li class="slash">/</li>
						<li><a href="<?php echo url.$category["sluq"]; ?>"><?php echo $category["title"]; ?></a></li>
					</ul>
				</div>
		</div>
	  </div>
	  <!-- Page Header -->
	  <!-- Blog List -->
	  <div class="container customBlogList">

	

		<?php 
		
							if(count($page)>0){
                    foreach ($page as $value) {
                        echo '
						<div class="blogDiv">
						 <div class="row">
							
							   <div class="col-md-4">
								  <div class="vAlign">
									 <a href="'.url.$value["sluq"].'"><div class="generalImage" style="background:url('.url.$value["image_file"].');"></div></a>
								  </div>
							   </div>
							   <div class="col-md-8">
								  <a href="'.url.$value["sluq"].'"><h3>'.$value["title"].'</h3></a> 
								  <ul>
										<li><i class="fas fa-user"></i> Yazar: '.$value["yazar"].'</li>
										<li><i class="far fa-clock"></i> Tarih: <span class="date"> '.$value["date"].'</span></li>
										<li><i class="fas fa-bars"></i> Kategori: '.$category["title"].'</li>
								  </ul>
								  <div class="clearfix"></div>
								  <p>'.$value["content"].'</p>
								  <a href="'.url.$value["sluq"].'" class="link">Devamını Oku</a>
							   </div>
							</div>
						 </div>
						';
					}
				}else{
							echo '<p style="    font-size: 16pt;
							padding-top: 20px;
							font-weight: bold;
							color: #b45c74;"><i class="fas fa-heart-broken"></i> Aradığınız Kriterlere Uygun İçerik Bulunamadı.</p>';
				}
		?>
		
	
		
		
		
		

<nav class="customPagination">

				<?php if(count($page)>0){ ?>
		  <ul class="pagination justify-content-center">
			<li class="page-item">
			  <a class="page-link" href="<?php echo $pages["link"]; ?>/1" tabindex="-1"><i class="fas fa-angle-double-left"></i></a>
			</li>

            <?php

					

							$j=($pages["active"]<5)?(10-$pages["active"]):5;
							$k=($pages["active"]<5)?1:($pages["active"]-4);
											for ($i=$k; $i <= $pages["count"] && $i < $pages["active"]+$j; $i++) { 
													if($pages["active"]==$i)
															echo '<li class="page-item active"><a class="page-link" href="'.$pages["link"].'/'.$i.'">'.$i.'</a></li>';
													else
															echo '<li class="page-item"><a class="page-link" href="'.$pages["link"].'/'.$i.'">'.$i.'</a></li>';   
                }
							
            ?>
			
			<li class="page-item">
			  <a class="page-link" href="<?php echo $pages["link"]; ?>/<?php echo $pages["count"]; ?>"><i class="fas fa-angle-double-right"></i></a>
			</li>
		  </ul>

						<?php } ?>
		</nav>
		<!-- Pagination -->
</div>