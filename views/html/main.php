 <!-- SLIDER -->
 <div id="carouselExampleIndicators" class="carousel slide customSlider" data-ride="carousel">           
            <div class="carousel-inner" role="listbox">
               
              <?php
              $active = "active"; 
              foreach ($slider as $key => $value) {

                 echo ' <div class="carousel-item '.$active.'" style="background-image: url('.url.$value["image_file"].')">
                 <div class="carousel-caption d-md-block">
                    <h1 class="animated fadeInDown slow">'.$value["s1"].'</h1>
                    <h4 class="animated fadeInDown slow">'.$value["s2"].'</h4>
                    <p class="animated fadeInDown slow">'.$value["s3"].'</p>
                          <a href="'.(($value["type-select"]==2)?(url.$value["page-sluq"]):$value["link"]).'" class="animated fadeInDownBig slower">DETAYLI BİLGİ</a> 	
                 </div>
              </div>';
              $active="";
              }
              
              ?>
             
               
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
         </div>
         <!-- SLIDER -->
         <!-- GENERAL CONTENT FOR ABOUT US AND SERVICIES -->
         <div class="container-fluid">         
            <div class="container" style="padding-top:50px;padding-bottom: 75px;background: white;">
                  <!-- ABOUT US -->
                  <div class="row">
                     <div class="col-md-12 customAbout">
                        <div class="row flex-column-reverse flex-md-row">
                           <div class="col-md-6">
                              <h1><?php echo $about1["title"]; ?></h1>
                              <p><?php echo $about1["content"]; ?></p>
                              <a href="<?php echo url.$about1["sluq"]; ?>" class="linkText">Devamını Oku</a>
                           </div>
                           <div class="col-md-6">
                              <div class="generalImage" style="background:url(<?php echo url.$about1["image_file"]; ?>);"></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- ABOUT US -->
                   <!-- SERVICIES -->
                   <div class="row" style="padding-top:50px;">
                        <div class="col-md-12 customAbout">
                           <div class="row">                              
                              <div class="col-md-6">
                                 <div class="generalImage" style="background:url(<?php echo url.$about2["image_file"]; ?>);"></div>
                              </div>
                              <div class="col-md-6">
                                    <h1><?php echo $about2["title"]; ?></h1>
                                    <p><?php echo $about2["content"]; ?></p>
                                    <div class="row">                                       
                                          <div class="col-md-4"><a href="<?php echo url.$static["k1-sluq"]; ?>" class="colorLink"><?php echo $static["k1"]; ?></a></div>
                                          <div class="col-md-4"><a href="<?php echo url.$static["k2-sluq"]; ?>" class="colorLink purpleLink"><?php echo $static["k2"]; ?></a></div>
                                          <div class="col-md-4"><a href="<?php echo url.$static["k3-sluq"]; ?>" class="colorLink"><?php echo $static["k3"]; ?></a></div>
                                    </div> 
                                    </div>                                      
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- SERVICIES -->
            </div>
         
         <!-- GENERAL CONTENT FOR ABOUT US AND SERVICIES -->
         <!-- EVENTS-->
         <div class="container-fluid customEvent">
            <div class="container">
               <div class="row">
                  <h2>DUYURULAR</h2>
                  <div id="demo" class="carousel slide" data-ride="carousel">

                        <!-- Indicators -->
                                             
                        <!-- The slideshow -->
                        <div class="carousel-inner">

                        <?php 
                        $active = "active";
                        foreach ($hizmetlerimiz as $key => $value) {
                           echo '  <div class="carousel-item '.$active.'">
                           <div class="row">
                              <div class="col-md-6">
                                   <div class="generalImage" style="background:url('.url.$value["image_file"].');"></div>
                              </div>
                              <div class="col-md-6">
                                   <a href="'.url.$value["sluq"].'"> <h4>'.$value["title"].'</h4></a>
                                 <p>'.$value["content"].' </p>
                              </div>
                           </div>
                         </div>';
                         $active="";
                        }
                        
                        ?>
                        
                       
                          
                        </div>
                      
                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                          <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                          <span class="carousel-control-next-icon"></span>
                        </a>
                      
                      </div>
               </div>
            </div>
         </div>
         <!-- EVENTS-->
         <!-- BLOG AND FORM BAND-->
            <div class="container customBlog">
               <!-- BLOG -->
               <div class="row customBlog">
                  <div class="col-md-12">
                        <h2>BİZDEN HABERLER</h2>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-12">
                    <p class="centerLink"> <a href="<?php echo url.$static["bottom2-sluq"]; ?>" class="link3">TÜM BLOG YAZILARI</a></p>
                  </div>
               </div>
               <div class="row">     
                  <?php 
                  
                  foreach ($blog as $key => $value) {
                     echo '  <div class="col-md-4">
                     <div class="generalImage" style="background:url('.url.$value["image_file"].')"></div>
                        
                        <a href="'.url.$value["sluq"].'" class="link">'.$value["title"].'</a>
                        <span class="date">'.$value["date"].'</span>
                        <a href="'.url.$value["sluq"].'" class="link2">Devamını Oku</a>
                     </div>';
                  }
                  ?>             
                   
                  </div>                                    
               <!-- BLOG -->
               <!-- FORM BAND -->
               <div class="row formBand">
                  <div class="col-md-12">
                     <h4>Bilgilerinizi Bırakın Sizi Arayalım :)</h4>
                     <p><?php echo $setting["contact"]; ?></p>
                     <a href="" data-toggle="modal" data-target="#myModal2" data-direction="right">BİLGİ FORMU</a>
                  </div>
               </div>
               <!-- FORM BAND -->
            </div>
         <!-- BLOG AND FORM BAND-->