 <!-- Page Header -->
 <div class="container-fluid customPageHeader">
		<div class="container">
		<h3><?php echo $page["title"]; ?></h3>
				<div class="breadCrumb">
					<ul>
					<li><a href="<?php echo url; ?>">Anasayfa</a></li>
						<li class="slash">/</li>
						<li><a href="<?php echo url.$category["sluq"]; ?>"><?php echo $category["title"]; ?></a></li>
					</ul>
				</div>
		</div>
	  </div>
	  <!-- Page Header -->
	<!-- Page Detail -->
	  <div class="container customPageDetail">
	  <div class="row flex-column-reverse flex-md-row">

 	<?php if($page["is_yazar-select"]==1){ $col=8; ?>

	  <div class="col-md-4">
	  <span class="menuBand"><?php echo $category["title"]; ?></span>
		<ul class="menuBandContent">
			<?php 
				foreach ($contents_list as $key => $value) {
					$active="";
					if($hTitle==$value["title"])
						$active ="menuActive";
					echo '<li><a class="'.$active.'" href="'.url.$value["sluq"].'">'.$value["title"].'</a></li>';
				}

				
			?>
			
		</ul>
	  </div>
			<?php }else{$col=12;} ?>
	  <div class="col-md-<?php echo $col;?>">
	  <!-- İçerik Tarih ve Yazar Adı -->
		<div class="row pageDetail">
			<div class="col-md-12">
			
				<ul>
					<?php
					if($page["is_date-select"]==1){
						echo '<li><i class="fas fa-user"></i> Yazar: '.$page["yazar"].'</li>';
						echo '<li><i class="far fa-clock"></i> <span class="date">'.$page["date"].'</span></li>';
					}
					?>
				</ul>
			</div>
		</div>
		<!-- İçerik Tarih ve Yazar Adı -->
		<!-- İçerik Resmi -->
		<div class="row pageContentImage">
			<div class="col-md-12">
			<div class="generalImage" style="background:url(<?php echo url.$page["image_file"]; ?>);"></div>			
			</div>
		</div>
		<!-- İçerik Resmi -->
		<!-- İçerik Yazısı -->
		<div class="row pageContent">
			<div class="col-md-12">
			
			<?php 
                    echo htmlspecialchars_decode($page["editor"]);
                ?>
			</div>
		</div>
		<!-- İçerik Yazısı -->
		</div>
</div>	
		<!-- Sosyal Medya Paylaşımları ve Etiketler -->
		<div class="row pageBlock">
			 <div class="col-md-6">
				 <?php echo $page["is_paylas-select"]==1?'<div class="socialShare ">
				<span><i class="fas fa-share-alt"></i></span>
				<ul>

				   <li><a style="cursor:pointer;" title="Facebok Paylaş" onclick="window.open(\'https://www.facebook.com/sharer/sharer.php?u=\'+encodeURIComponent(location.href),\'facebook-share-dialog\',\'width=626,height=436\');return false;"><i class="fab fa-facebook-square"></i></a></li>

				  <li><a style="cursor:pointer;" title="Twitter Paylaş" onclick="window.open(\'https://twitter.com/share?url=\'+encodeURIComponent(location.href),\'facebook-share-dialog\',\'width=626,height=436\');return false;"><i class="fab fa-twitter"></i></a></li>

              <li><a style="cursor:pointer;" title="Google+ Paylaş" onclick="window.open(\'https://plus.google.com/share?url=\'+encodeURIComponent(location.href),\'facebook-share-dialog\',\'width=626,height=436\');return false;"><i class="fab fa-google-plus-g"></i></a></li>

              <li><a style="cursor:pointer;" title="Linkedin Paylaş" onclick="window.open(\'http://www.linkedin.com/shareArticle?url=\'+encodeURIComponent(location.href),\'facebook-share-dialog\',\'width=626,height=436\');return false;"><i class="fab fa-linkedin"></i></a></li>           

				</ul>
            </div>':''; ?>
            
          </div>
          <div class="col-md-6">
			  <?php if($page["is_label-select"]==1){ ?>
				<div class="cloudTags">
				<span><i class="fas fa-tag"></i></span>
				<ul>
				<?php 
						if(!empty($page["labels"])){
							foreach (explode(",",$page["labels"]) as $value) {
								echo ' <li><a href="'.url.'news/'.main::slugify($value).'">'.$value.'</a></li>';
							}
						}
						
					
					?>
				</ul>
				</div>
					<?php }?>
          </div>			
		</div>
		<!-- Sosyal Medya Paylaşımları ve Etiketler -->
		<?php if($page["is_yazar-select"]==1){ ?>
		<!-- Diğer Yazı, İçerik veya Kullanıcı Sliderı -->
		<div class="row otherBlogText">
			<div class="col-md-12">
				<h3>Diğer Yazılar</h3>
			</div>							
		</div>
		<div class="row customBlog"> 
                  <div class="carouselBlog owl-carousel owl-theme">
				
						
				
				
				<?php 
					foreach ($contents as $key => $value) {
						
						echo '
						<div class="col-md-12">
							<a href="'.url.$value["sluq"].'"><div class="generalImage" style="background:url('.url.$value["image_file"].')"></div></a>
							<a href="'.url.$value["sluq"].'" class="link">'.$value["title"].'</a>
							<span class="date">'.$value["date"].'</span>
							<a href="'.url.$value["sluq"].'" class="link2">Devamını Oku</a>
						</div>';
					}
				
				?>			
		  </div>	
         </div>
		 <!-- Diğer Yazı, İçerik veya Kullanıcı Sliderı -->
		 <?php } ?>
	  </div>
			
	  
           
