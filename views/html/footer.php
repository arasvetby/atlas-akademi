         <!-- FOOTER -->
         <div class="container-fluid customFooter">
            <div class="container">
               <div class="row">
                  <div class="col-md-3 footerLogo">
                     <img src="<?php echo url.$setting["logo_file"]; ?>" alt="">
                     <p><?php echo $setting["content"]; ?></p>
                  </div>
                  <div class="col-md-3 footerMenu">
                     <h2>HIZLI MENÜ</h2>
                     <ul>
                     <?php 
                               
                               foreach ($menu as $value) {
                                    if ($value["level-select"]!=2){
                                     if ($value["type-select"]==1) {
                                             echo ' <li><a  href="'.$value["link"].'">'.$value["title"].'</a></li>';
                                     }else if ($value["type-select"]==2){
                                           echo ' <li><a href="'.$value["sluq"].'">'.$value["title"].'</a></li>';
                                     }else{
                                           echo ' <li><a  href="'.url.$value["category-sluq"].'">
                                                       '.$value["category-option"][$value["category-select"]].'</a></li>';
                                     }
                                 }
                                    
                               }
                         ?>
                     </ul>
                  </div>
                  <div class="col-md-3 footerSocial">
                        <a href="tel:<?php echo $setting["phone"]; ?>"><h3><?php echo $setting["phone"]; ?></h3></a>   
                        <span class="tLine1"><?php echo $setting["saat"]; ?></span>
                        <span class="tLine2">E-Mail Adresimiz:</span>
                        <a href="tel:<?php echo $setting["mail"]; ?>"><span class="tLine3"><?php echo $setting["mail"]; ?></span></a> 
                        <span class="tLine4">Adresimiz:</span>
                        <span class="tLine5"><?php echo $setting["address"]; ?></span>                                    
                  </div>
                  <div class="col-md-3 footerSocial">
                        <h2>FACEBOOK'DA BİZİ TAKİP EDİN</h2>
                        <?php echo $social["iFacebook"]; ?>
                  </div>
               </div>
               <div class="row">
                     <div class="col-md-12">
                        <p class="footerCopyright">Tüm Hakları Saklıdır. 2019 Yeditepe Atlas Özel Eğitim ve Rehabilitasyon Merkezi</p>
                     </div>
                  </div>
            </div>
         </div>
         <!-- FOOTER -->        
         <!-- MODAL -->
	  <div class="customModal">
         <div class="modal right fade " id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
            <div class="modal-dialog" role="document">
               <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <div class="modal-body">
                     <img src="<?php echo url.$setting["logo_file"]; ?>">
                     <p>ATLAS AKADEMİ BİLGİ FORMU</p>
                     <form>
                       <div class="form-group">
                        <label for="adSoyad">Adınız ve Soyadınız</label>
                        <input type="text" class="form-control" name="adSoyad" id="adSoyad" placeholder="Adınız ve Soyadınız" required>						
                       </div>
                       <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="E-mailiniz" required>
                       </div>
                       <div class="form-group">
                        <label for="telefon">Telefon</label>
                        <input type="phone" class="form-control" name="telefon" id="telefon" placeholder="5xx xxx xx xx">
                       </div>                       
                       <a type="submit" >Kayıt Ol</a>
                     </form>
                  </div>
      
               </div>
            </div>
         </div>
         </div>
           <!-- MODAL -->
        
           <script src="<?php echo assets; ?>js/jquery-3.3.1.min.js"></script>    
           <script src="<?php echo assets; ?>js/bootstrap/bootstrap.min.js"></script>
           <script src="<?php echo assets; ?>css/owl/js/owl.carousel.min.js"></script>
           <script src="<?php echo assets; ?>css/ekko/js/ekko-lightbox.min.js"></script>
           <script src="<?php echo assets; ?>js/custom.js"></script>
         
			<script>
					 $( document ).ready(function() {
		$( ".date" ).each(function() {
    var data =$(this).html();
	if(data == " ") {
	$(this).html("Tarih: 18 Aralık 2018");
} else {

	var arr = data.split('/');
	
	switch (arr[1]) { 
	case '01': 
		arr[1] = "Ocak";
		break;
	case '02': 
		arr[1] = "Şubat";
		break;
	case '03': 
		arr[1] = "Mart";
		break;
	case '04': 
		arr[1] = "Nisan";
		break;
	case '05': 
		arr[1] = "Mayıs";
		break;
    case '06': 
		arr[1] = "Haziran";
		break;
    case '07': 
		arr[1] = "Temmuz";
		break;
    case '08': 
		arr[1] = "Ağustos";
		break;
    case '09': 
		arr[1] = "Eylül";
		break;
    case '10': 
		arr[1] = "Ekim";
		break;
    case '11': 
		arr[1] = "Kasım";
		break;
    case '12': 
		arr[1] = "Aralık";
		break;		
}
	switch (arr[0]) { 
	case '01': 
		arr[0] = "1";
		break;
	case '02': 
		arr[0] = "2";
		break;
	case '03': 
		arr[0] = "3";
		break;
	case '04': 
		arr[0] = "4";
		break;
	case '05': 
		arr[0] = "5";
		break;
	case '06': 
		arr[0] = "6";
		break;
	case '07': 
		arr[0] = "7";
		break;
	case '08': 
		arr[0] = "8";
		break;
	case '09': 
		arr[0] = "9";
		break;	
}		
	$(this).html("Tarih: " + arr[0] + " " + arr[1] + " " +arr[2] );}
  });   													
		});

		$(".card .card-text").text(function(index, currentText) {
            return currentText.substr(0, 120) + '...';
				});
		$("#tikla").click(function(){
			$('#myModal2').modal('show');
		});		
			</script>
</body>
</html>
