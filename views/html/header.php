<!DOCTYPE html>
<html lang="tr">
      <head>
            <meta charset="UTF-8">
            <title><?php echo ((isset($hTitle))?$hTitle:$setting["title"])." | ".$setting["slogan"];?></title>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="author" content="<?php echo $meta_author;?>">
            <meta property="og:type" content="<?php echo $meta_type;?>">
            <meta property="og:title" content="<?php echo ((isset($hTitle))?$hTitle:$setting["title"])." | ".$setting["slogan"];?>">
            <meta property="og:image" content="<?php echo url.$meta_image;?>">
            <meta property="og:locale" content="tr_TR">
            <meta property="og:url" content="<?php echo $meta_url;?>">
            <link rel="canonical" href="<?php echo $meta_url;?>">
            <meta name="keywords" content="<?php echo $meta_keywords;?>">
            <meta name="description" content="<?php echo $meta_description;?>">            
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <link rel="icon" href="<?php echo assets; ?>images/fav.png" sizes="32x32" />                        
            <link rel="stylesheet" href="<?php echo assets; ?>css/bootstrap/bootstrap.min.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/animate/animate.min.css">   
            <link rel="stylesheet" href="<?php echo assets; ?>css/topHeader/style.css">         
            <link rel="stylesheet" href="<?php echo assets; ?>css/menu/style.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/slider/style.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/about/style.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/event/style.css"> 
            <link rel="stylesheet" href="<?php echo assets; ?>css/blog/style.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/footer/style.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/rightSidebar/style.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/pageHeader/style.css">  
            <link rel="stylesheet" href="<?php echo assets; ?>css/blogList/style.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/pagination/style.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/categoryList/style.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/pageDetail/style.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/contact/style.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/gallery/style.css">                                                   
            <link rel="stylesheet" href="<?php echo assets; ?>css/owl/css/owl.carousel.min.css">
            <link rel="stylesheet" href="<?php echo assets; ?>css/owl/css/owl.theme.default.min.css">            
            <link rel="stylesheet" href="<?php echo assets; ?>css/ekko/css/ekko-lightbox.css">                     
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
            <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,600,700&amp;subset=latin-ext" rel="stylesheet">
            <style>
                  body {
                     font-family: 'Source Sans Pro', sans-serif !important;
                     background-image: url(<?php echo assets; ?>images/bg.jpg)
                  }
                  .generalImage {
                     width: 100%;
                     background-size: cover !important;
                     background-position: center !important;
                  }
            </style>
        </head>
<body>
      <!-- TOP HEADER -->
      <div class="container-fluid topHeader">
        <div class="container">
            <div class="row">
                
                <div class="col-md-12 pull-right tHSocial">
                    <ul>
                        <li><a href="<?php echo $social["youtube"];?>" target="_blank" title="Youtube'da Beni Takip Etmek İster misiniz ?"><i class="fab fa-youtube-square"></i></a></li>
                        <li><a href="<?php echo $social["instagram"];?>" target="_blank" title="Instagram'da Beni Takip Etmek İster misiniz ?"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="<?php echo $social["twetter"];?>" target="_blank" title="Twitter'da Beni Takip Etmek İster misiniz ?"><i class="fab fa-twitter-square"></i></a></li>
                        <li><a href="<?php echo $social["facebook"];?>" target="_blank" title="Facebook'da Beni Takip Etmek İster misiniz ?"><i class="fab fa-facebook-square"></i></a></li>                                                                        
                    </ul>
                </div>
            </div>
        </div>        
    </div>
    <!-- TOP HEADER -->   
    <!-- MENU -->    
    <nav class="navbar navbar-expand-lg  static-top " style="background: white;">
            <div class="container"> 
               <div class="row">
                  <div class="col-md-3 col-xs-12">                  
                     <a class="navbar-brand" href="<?php echo url; ?>">
                        <img src="<?php echo url.$setting["logo_file"]; ?>" alt="" width="100%;">
                     </a>
                     
                  </div> 
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                     <i class="fas fa-bars"></i>
               </button> 
                  <div class="col-md-9">
                     <div class="row">
                        <div class="col-md-12">
                              <div class="menuRow1 dnone">
                                 <ul>
                                    <li><a href="mailto:<?php echo $setting["mail"]; ?>"><i class="fas fa-envelope"></i> <?php echo $setting["mail"]; ?></a></li>
                                    <li><a href="tel:<?php echo $setting["phone"]; ?>"><i class="fas fa-phone"></i><?php echo $setting["phone"]; ?></a></li>
                                    <li><a href="#"  data-toggle="modal" data-target="#myModal2" data-direction="right" class="menuRow1Button"><i class="far fa-smile-beam"></i> Sorularınız İçin Arayalım</a></li>
                                 </ul>
                              </div>
                        </div>
                     </div>
                     <div class="row">
                           <div class="col-md-12">
                                 <div class="collapse navbar-collapse thMenu" id="navbarResponsive">
                                       <ul class="navbar-nav">
                                       <?php
                                          foreach ($menu as $key =>$value) {
                                             $active ="";
                                             
                                             $status =array_search($value["no"],array_column($sub_link,'no'))>-1?false:true;
                                             if($value["level-select"]==2)
                                                continue;

                                             if ($value["type-select"]==1 && $status) {
                                                   $active=($active_link==$value["link"])?"active":"";
                                                   echo ' <li class="nav-item '.$active.'">
                                                      <a class="nav-link " href="'.$value["link"].'">'.$value["title"].'
                                                     
                                                      </a>
                                                   </li>';
                                             }else if ($value["type-select"]==2 && $status){
                                                $active=($active_link==url.$value["page-sluq"])?"active":"";
                                                echo ' <li class="nav-item '.$active.'">
                                                      <a class="nav-link" href="'.url.$value["page-sluq"].'">'.$value["page"].'
                                                      </a>
                                                   </li>';
                                             }else if($value["type-select"]==3 && $status){
                                                if(${"type_".$value["category-select"]}["type-select"]==1 || ${"type_".$value["category-select"]}["type-select"]==3){
                                                   $active=($active_link==url.$value["category-sluq"])?"active":"";
                                                   echo ' <li class="nav-item '.$active.'">
                                                      <a class="nav-link " href="'.url.$value["category-sluq"].'">'.$value["title"].'
                                                    
                                                      </a>
                                                   </li>';
                                                }else{
                                                   foreach (${"menu_".$value["category-select"]} as $option) {
                                                      if($active_link==url.$value["category-sluq"] || $active_link == url.$option["sluq"]) $active = "active";
                                                      
                                                   }
                                                      
                                                   echo ' <li class="nav-item dropdown '.$active.'" >
                                                            <a class="nav-link dropdown-toggle" href="'.url.$value["category-sluq"].'" id="menu-'.$value["category-select"].'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            '.$value["category-option"][$value["category-select"]].'
                                                            </a>
                                                            <div class="dropdown-menu" aria-labelledby="menu-'.$value["category-select"].'">
                                                               ';
                                                                  foreach (${"menu_".$value["category-select"]} as $option) {
                                                                     $active="";
                                                                     if($active_link==url.$value["category-sluq"] || $active_link == url.$option["sluq"]) $active = "active";
                                                                     echo '<a class="dropdown-item '.$active.'" href="'.url.$option["sluq"].'">'.$option["title"].'</a>';
                                                                  }
                                                               echo ' 
                                                            </div>
                                                         </li>';
                                                }
                                                
                                             }else {
                                                foreach ($sub_link as $option) {
                                                   if($value["no"]==$option["no"] && ($active_link==url.$value["sluq"] || $active_link == $option["link"])) $active = "active";
                                                   
                                                }
                                                echo ' <li class="nav-item dropdown '.$active.'" >
                                                   <a class="nav-link dropdown-toggle" href="'.url.$value["sluq"].'" id="menu-'.$value["no"].'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                   '.$value["title"].'
                                                   </a>
                                                   <div class="dropdown-menu" aria-labelledby="menu-'.$value["no"].'">
                                                      ';
                                                      foreach ($sub_link as $option) {

                                                         if ($value["no"]==$option["no"]){
                                                            $active="";
                                                            if($active_link==url.$value["sluq"] || $active_link == $option["link"]) $active = "active";

                                                            echo '<a class="dropdown-item '.$active.'" href="'.$option["link"].'">'.$option["title"].'</a>';
                                                         }
                                                      }
                                                   echo ' 
                                                   </div>
                                                </li>';
                                             }
                                             
                                             
                                          }
                                    
                                    
                                    
                                    ?>
                                        
                                       </ul>
                                       <div class="mobileMenu">
                                         <ul>
                                                 <li><a href="mailto:<?php echo $setting["mail"]; ?>"><i class="fas fa-envelope"></i></a></li>
                                                 <li><a href="tel:<?php echo $setting["phone"]; ?>"><i class="fas fa-phone"></i></a></li>       
                                       
                                                 <li><a href="<?php echo $social["youtube"];?>" target="_blank" title="Youtube'da Beni Takip Etmek İster misiniz ?"><i class="fab fa-youtube-square"></i></a></li>
                                                <li><a href="<?php echo $social["instagram"];?>" target="_blank" title="Instagram'da Beni Takip Etmek İster misiniz ?"><i class="fab fa-instagram"></i></a></li>
                                                <li><a href="<?php echo $social["twetter"];?>" target="_blank" title="Twitter'da Beni Takip Etmek İster misiniz ?"><i class="fab fa-twitter-square"></i></a></li>
                                                <li><a href="<?php echo $social["facebook"];?>" target="_blank" title="Facebook'da Beni Takip Etmek İster misiniz ?"><i class="fab fa-facebook-square"></i></a></li>                                                                                                                                  
                                         </ul>
                                     </div>
                                    </div>
                                 </div>
                           </div>
                        </div>
                  </div>
               </div>
                             
               
         </nav>
         <!-- MENU -->