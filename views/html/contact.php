<!-- Page Header -->
<div class="container-fluid customPageHeader">
		<div class="container">
			<h3>İletişim</h3>
				<div class="breadCrumb">
					<ul>
						<li><a href="#">Anasayfa</a></li>
						<li class="slash">/</li>
						<li><a href="#">İletişim</a></li>
					</ul>
				</div>
		</div>
	  </div>
	  <!-- Page Header -->
	  <!-- Contact --> 
			<div class="container customContact">
				
				<div class="row">													
					<div class="col-md-4">
						<p><i class="fas fa-home"></i><?php echo $setting["address"]; ?></p>
					</div>
					<div class="col-md-4">
						<p><a href="tel:<?php echo $setting["phone"]; ?>"><i class="fas fa-phone"></i> <?php echo $setting["phone"]; ?></a></p>
						<p><a href="mailto:<?php echo $setting["mail"]; ?>"><i class="fas fa-envelope"></i> <?php echo $setting["mail"]; ?></a></p>
					</div>
					<div class="col-md-4">
						<span><i class="fas fa-map-marked-alt"></i></span>
					</div>
				</div>
				<div class="row">												
					<div class="col-md-8">
					<div class="row"><div class="col-md-12"><img src="<?php echo assets; ?>images/iletisim.jpg" alt="" style="margin-bottom:10px;border-radius:10px;"/></div></div>	
						<form>
						  <div class="form-group">
							<label>Adınız ve Soyadınız</label>
							<input type="text" class="form-control" name="isim" placeholder="Lütfen Adınızı ve Soyadınızı Yazınız" required>							
						  </div>
						  <div class="form-group">
							<label>E-mail</label>
							<input type="email" class="form-control" name="email" placeholder="Lütfen Mail Adresini Yazınız" required>
						  </div>
						  <div class="form-group">
							<label>Telefon</label>
							<input type="tel" class="form-control" name="tel" placeholder="Lütfen Telefon Numaranızı Yazınız" required>
						  </div>
						  <div class="form-group">
							<label>Konu</label>
							<input type="text" class="form-control" name="konu" placeholder="Lütfen Ulaşma Konunuzu Yazınız" required>
						  </div>
						  <div class="form-group">
							<label>Mesajınız</label>
							<textarea class="form-control" rows="4" name="mesaj" placeholder="Lütfen Mesajınızı Yazınızı"></textarea>
						  </div>
						  <a type="submit" class="btn btn-primary btn-block">Gönder</a>
						</form>
					</div>
					<div class="col-md-4">
					<?php echo $setting["maps"]; ?>
					</div>					
				</div>
			</div>	  
	  <!-- Contact -->