 <!-- Page Header -->
 <div class="container-fluid customPageHeader">
		<div class="container">
		<h3><?php echo $page["title"]; ?></h3>
				<div class="breadCrumb">
					<ul>
					<li><a href="<?php echo url; ?>">Anasayfa</a></li>
						<li class="slash">/</li>
						<li><a href="<?php echo url.$category["sluq"]; ?>"><?php echo $category["title"]; ?></a></li>
					</ul>
				</div>
		</div>
	  </div>
	  <!-- Page Header -->
      <!-- Gallery -->
      
      <div class="container customCList customGallery">
		 
         <div class="row">		
       
         <?php
              
              foreach (json_decode($page["gallery"],true) as $key => $value) {
                  echo '<div class="col-md-2">
                   <a href="'.url.$value["src"].'" data-toggle="lightbox" data-gallery="galeri-adi" data-title="'.$value["title"].'" data-footer="'.$value["content"].'">
                     <div class="generalImage" style="background:url('.url.$value["src"].');"></div>
                   </a>
                </div>';
              }
              

               
            ?>
	  		   

         </div>
       
      </div>

		