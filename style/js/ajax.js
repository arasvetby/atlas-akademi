




function GetPage(path ,id){
    $.ajax({url: path, success: function(result){
            
        if (id==="modal") 
            $("#table-modal").modal();

        $("#"+id).html(result);
       
    }});

    
}


function SavePage(path,id,type="table",method="post"){
    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }
   
    $.ajax({
        url: path,
        data: $('#'+id).serialize(),
        type: method,
        success: function (result) {
            
            $.notify({
                icon: 'glyphicon glyphicon-ok-sign',
                message: "Kayıt işlemi başarılı." 
            },{
                type: 'success'
            });
            
           $("#"+type).html(result);
            

        },
        error: function (result, status, error) {
            $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                message: result.responseText
            },{
                type: 'danger'
            });
            
          

        }
    });
    
    
}

function SourcePage(path,id,type="Source",method="post"){
    
    if (type==="Reset") {
        $('#'+id)[0].reset();
    }
    
    $.ajax({
        url: path,
        data: $('#'+id).serialize(),
        type: method,
        success: function (result) {
            
            $.notify({
                icon: 'glyphicon glyphicon-ok-sign',
                message: "Filtre işlemi başarılı." 
            },{
                type: 'success'
            });
            
            $("#table").html(result);
            

        },
        error: function (result, status, error) {
            $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                message: result.responseText
            },{
                type: 'danger'
            });
            
          

        }
    });
    
    
}


function DeleteData(path,id="table"){
    
    swal({
        title: "Silmek istediğinize emin misiniz?",
        text: "Bu kayıt kalıcı olarak silinecektir. Onaylıyor musunuz!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            
          
          $.ajax({
            url: path,
            data: null,
            type: "get",
            success: function (result) {

                swal("Kayıt başarıyla silindi!", {
                    icon: "success",
                  });

               $("#"+id).html(result);


            },
            error: function (result, status, error) {
                swal("Silme işlemi başarısız!");

            }
        });
      
            
            
          
        } else {
          swal("Silme işlemi iptal edildi!");
        }
      });
    
    
}

function StatusData(path){
    
   
          
          $.ajax({
            url: path,
            data: null,
            type: "get",
            success: function (result) {

                $.notify({
                    icon: 'glyphicon glyphicon-ok-sign',
                    message: "Durum güncellendi." 
                },{
                    type: 'success'
                });

               $("#table").html(result);


            },
            error: function (result, status, error) {
                $.notify({
                    icon: 'glyphicon glyphicon-info-sign',
                    message: result.responseText
                },{
                    type: 'danger'
                });

            }
        });
    
    
    
}

function SaveOrder(path,no){
    
          data= {
              "detail":no,
              "order":$("#orderby_"+no).val()
          };
          
          $.ajax({
            url: path,
            data: data,
            type: "post",
            success: function (result) {

                $.notify({
                    icon: 'glyphicon glyphicon-ok-sign',
                    message: "Görünüm sırası güncellendi." 
                },{
                    type: 'success'
                });

               $("#table").html(result);


            },
            error: function (result, status, error) {
                $.notify({
                    icon: 'glyphicon glyphicon-info-sign',
                    message: result.responseText
                },{
                    type: 'danger'
                });

            }
        });
    
    
    
}


 function PostMedia(path,id) {

    var form = $('#upload-media')[0];
    var data = new FormData(form);

   
    $.ajax({
        url: path,
        data: data,
        type: "post",
        contentType: 'application/json; charset=utf-8',
        processData: false,
        contentType: false,
        success: function (result) {
            $.notify({
                icon: 'glyphicon glyphicon-ok-sign',
                message: "Medya başarıyla yüklendi." 
            },{
                type: 'success'
            });
            $("#"+id).html(result);
        },
        error: function (result, status, error) {
            $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                message: result.responseText
            },{
                type: 'danger'
            });
        }
    });

}




  
  function sluq(text) {
            var trMap = {
                'çÇ': 'c',
                'ğĞ': 'g',
                'şŞ': 's',
                'üÜ': 'u',
                'ıİ': 'i',
                'öÖ': 'o'
            };
            for (var key in trMap) {
                text = text.replace(new RegExp('[' + key + ']', 'g'), trMap[key]);
            }
            return text.replace(/[^-a-zA-Z0-9\s]+/ig, '') // remove non-alphanumeric chars
                .replace(/\s/gi, "-") // convert spaces to dashes
                .replace(/[-]+/gi, "-") // trim repeated dashes
                .toLowerCase();

  }