 function imageError(element) {
            element.onerror = '';
            element.src = '{{media}}no-image.png';
    }
         /*SAYFA YÜKLENİNCE GALERİ MODAL DIV YÜKSEKLİĞİ İÇİN*/

   $(document).ready(function () {
        
        mediaHeight();
        mediaSelect();
    });
    /*SAYFA YÜKLENİNCE GALERİ MODAL DIV YÜKSEKLİĞİ İÇİN*/

    /*BROWSER RESIZE OLUR İSE MEDYA GALERİ MODAL STABİLETESİ İÇİN*/
    $(window).resize(function () {
        mediaHeight();
    });
    /*BROWSER RESIZE OLUR İSE MEDYA GALERİ MODAL STABİLETESİ İÇİN*/

    /*MEDYA GALERİ MODAL'DA AÇILIYOR İSE DIV YÜKSEKLİK AYARLARI*/
    function mediaHeight() {
        var pencereYukseklik = $(window).height();
        var filtersYukseklik = $(".mediaJS .mediaFilters").height();
        $(".mediaJS .imageGalleryPadding").css("height", pencereYukseklik - 341);
        $(".mediaJS .modalImageDetails").css("height", pencereYukseklik - 280);
        
    };
    /*MEDYA GALERİ MODAL'DA AÇILIYOR İSE DIV YÜKSEKLİK AYARLARI*/

    /*MEDYA GALERİ RESİM SEÇİMİ AKTİF PASİF İŞLEMLERİ*/
    function mediaSelect(){
        $(".mediaBorder").click(function () {
            //Border İşlemler
            $(".mediaBorder").removeClass("mediaBorderActive");
            $(this).addClass("mediaBorderActive");
            // Seçilen Resim Özellikleri
            var baslik = $(this).children("span").html();
            var title = $(this).children("img").attr("title");
            var alt = $(this).children("img").attr("alt");
            var genislik = $(this).children("img").attr("width");
            var yukseklik = $(this).children("img").attr("height");
            var imageUrl = $(this).children("img").attr("url");
            var imageSrc = $(this).children("img").attr("src");
            var imageNo = $(this).children("img").attr("imageno");
            var imageCreate = $(this).children("img").attr("create");
            var imageSize = $(this).children("img").attr("size");
            var detail = $(this).children("img").attr("detail");
            $("#imageDetails input[name='name']").val(baslik);
            $("#imageDetails input[name='title']").val(title);
            $("#imageDetails input[name='alt']").val(alt);
            $("#imageDetails input[name='width']").val(genislik);
            $("#imageDetails input[name='height']").val(yukseklik);
            $("#imageDetails input[name='url']").val(imageUrl);
            $("#imageDetails input[name='no']").val(imageNo);
            $("#imageDetails input[name='detail']").val(detail);
            $("#imageDetails #baslik").html(baslik);
            $("#imageDetails #tarih").html(imageCreate);
            $("#imageDetails #boyut").html((imageSize/1024).toFixed() +'KB');
            $("#imageDetails #resimBoyut").html(genislik + " x " + yukseklik);
            $("#imageDetails #secilenResim").attr("src", imageSrc);
            $("#imageDetails #resimSil").attr("onclick", "DeleteData('{{url}}?controller=media&path=delete&detail="+detail+"','media-main')");
            $("#imageDetails .imageDetailsHide").show();
        });
    }
    /*MEDYA GALERİ RESİM SEÇİMİ AKTİF PASİF İŞLEMLERİ*/

   
    
    /* Media Seçme İşlemi*/
    function SelectMedia() {
        $("#selectMedia").val($("#imageDetails input[name='no']").val());
    }
    /* Media Seçme İşlemi*/

    $(".sidebar-menu li").click(function() {
      $(".sidebar-menu li").removeClass("active");
      $(this).addClass("active");
    });